import gql from 'graphql-tag'
import * as Urql from 'urql'
export type Maybe<T> = T | null
export type InputMaybe<T> = Maybe<T>
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K]
}
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>
}
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>
}
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
  date: any
  timestamptz: any
  uuid: any
}

export type Boolean_Cast_Exp = {
  String?: InputMaybe<String_Comparison_Exp>
}

/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
  _cast?: InputMaybe<Boolean_Cast_Exp>
  _eq?: InputMaybe<Scalars['Boolean']>
  _gt?: InputMaybe<Scalars['Boolean']>
  _gte?: InputMaybe<Scalars['Boolean']>
  _in?: InputMaybe<Array<Scalars['Boolean']>>
  _is_null?: InputMaybe<Scalars['Boolean']>
  _lt?: InputMaybe<Scalars['Boolean']>
  _lte?: InputMaybe<Scalars['Boolean']>
  _neq?: InputMaybe<Scalars['Boolean']>
  _nin?: InputMaybe<Array<Scalars['Boolean']>>
}

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['String']>
  _gt?: InputMaybe<Scalars['String']>
  _gte?: InputMaybe<Scalars['String']>
  /** does the column match the given case-insensitive pattern */
  _ilike?: InputMaybe<Scalars['String']>
  _in?: InputMaybe<Array<Scalars['String']>>
  /** does the column match the given POSIX regular expression, case insensitive */
  _iregex?: InputMaybe<Scalars['String']>
  _is_null?: InputMaybe<Scalars['Boolean']>
  /** does the column match the given pattern */
  _like?: InputMaybe<Scalars['String']>
  _lt?: InputMaybe<Scalars['String']>
  _lte?: InputMaybe<Scalars['String']>
  _neq?: InputMaybe<Scalars['String']>
  /** does the column NOT match the given case-insensitive pattern */
  _nilike?: InputMaybe<Scalars['String']>
  _nin?: InputMaybe<Array<Scalars['String']>>
  /** does the column NOT match the given POSIX regular expression, case insensitive */
  _niregex?: InputMaybe<Scalars['String']>
  /** does the column NOT match the given pattern */
  _nlike?: InputMaybe<Scalars['String']>
  /** does the column NOT match the given POSIX regular expression, case sensitive */
  _nregex?: InputMaybe<Scalars['String']>
  /** does the column NOT match the given SQL regular expression */
  _nsimilar?: InputMaybe<Scalars['String']>
  /** does the column match the given POSIX regular expression, case sensitive */
  _regex?: InputMaybe<Scalars['String']>
  /** does the column match the given SQL regular expression */
  _similar?: InputMaybe<Scalars['String']>
}

export type Date_Cast_Exp = {
  String?: InputMaybe<String_Comparison_Exp>
}

/** Boolean expression to compare columns of type "date". All fields are combined with logical 'AND'. */
export type Date_Comparison_Exp = {
  _cast?: InputMaybe<Date_Cast_Exp>
  _eq?: InputMaybe<Scalars['date']>
  _gt?: InputMaybe<Scalars['date']>
  _gte?: InputMaybe<Scalars['date']>
  _in?: InputMaybe<Array<Scalars['date']>>
  _is_null?: InputMaybe<Scalars['Boolean']>
  _lt?: InputMaybe<Scalars['date']>
  _lte?: InputMaybe<Scalars['date']>
  _neq?: InputMaybe<Scalars['date']>
  _nin?: InputMaybe<Array<Scalars['date']>>
}

/** columns and relationships of "education" */
export type Education = {
  __typename?: 'education'
  createdAt: Scalars['timestamptz']
  degree?: Maybe<Scalars['String']>
  description?: Maybe<Scalars['String']>
  fieldOfStudy?: Maybe<Scalars['String']>
  from: Scalars['date']
  id: Scalars['uuid']
  isDeleted: Scalars['Boolean']
  school?: Maybe<Scalars['String']>
  to?: Maybe<Scalars['date']>
  updatedAt: Scalars['timestamptz']
}

/** aggregated selection of "education" */
export type Education_Aggregate = {
  __typename?: 'education_aggregate'
  aggregate?: Maybe<Education_Aggregate_Fields>
  nodes: Array<Education>
}

/** aggregate fields of "education" */
export type Education_Aggregate_Fields = {
  __typename?: 'education_aggregate_fields'
  count: Scalars['Int']
  max?: Maybe<Education_Max_Fields>
  min?: Maybe<Education_Min_Fields>
}

/** aggregate fields of "education" */
export type Education_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Education_Select_Column>>
  distinct?: InputMaybe<Scalars['Boolean']>
}

/** Boolean expression to filter rows from the table "education". All fields are combined with a logical 'AND'. */
export type Education_Bool_Exp = {
  _and?: InputMaybe<Array<Education_Bool_Exp>>
  _not?: InputMaybe<Education_Bool_Exp>
  _or?: InputMaybe<Array<Education_Bool_Exp>>
  createdAt?: InputMaybe<Timestamptz_Comparison_Exp>
  degree?: InputMaybe<String_Comparison_Exp>
  description?: InputMaybe<String_Comparison_Exp>
  fieldOfStudy?: InputMaybe<String_Comparison_Exp>
  from?: InputMaybe<Date_Comparison_Exp>
  id?: InputMaybe<Uuid_Comparison_Exp>
  isDeleted?: InputMaybe<Boolean_Comparison_Exp>
  school?: InputMaybe<String_Comparison_Exp>
  to?: InputMaybe<Date_Comparison_Exp>
  updatedAt?: InputMaybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "education" */
export enum Education_Constraint {
  /** unique or primary key constraint on columns "id" */
  EducationPkey = 'education_pkey'
}

/** input type for inserting data into table "education" */
export type Education_Insert_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>
  degree?: InputMaybe<Scalars['String']>
  description?: InputMaybe<Scalars['String']>
  fieldOfStudy?: InputMaybe<Scalars['String']>
  from?: InputMaybe<Scalars['date']>
  id?: InputMaybe<Scalars['uuid']>
  isDeleted?: InputMaybe<Scalars['Boolean']>
  school?: InputMaybe<Scalars['String']>
  to?: InputMaybe<Scalars['date']>
  updatedAt?: InputMaybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Education_Max_Fields = {
  __typename?: 'education_max_fields'
  createdAt?: Maybe<Scalars['timestamptz']>
  degree?: Maybe<Scalars['String']>
  description?: Maybe<Scalars['String']>
  fieldOfStudy?: Maybe<Scalars['String']>
  from?: Maybe<Scalars['date']>
  id?: Maybe<Scalars['uuid']>
  school?: Maybe<Scalars['String']>
  to?: Maybe<Scalars['date']>
  updatedAt?: Maybe<Scalars['timestamptz']>
}

/** aggregate min on columns */
export type Education_Min_Fields = {
  __typename?: 'education_min_fields'
  createdAt?: Maybe<Scalars['timestamptz']>
  degree?: Maybe<Scalars['String']>
  description?: Maybe<Scalars['String']>
  fieldOfStudy?: Maybe<Scalars['String']>
  from?: Maybe<Scalars['date']>
  id?: Maybe<Scalars['uuid']>
  school?: Maybe<Scalars['String']>
  to?: Maybe<Scalars['date']>
  updatedAt?: Maybe<Scalars['timestamptz']>
}

/** response of any mutation on the table "education" */
export type Education_Mutation_Response = {
  __typename?: 'education_mutation_response'
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']
  /** data from the rows affected by the mutation */
  returning: Array<Education>
}

/** on_conflict condition type for table "education" */
export type Education_On_Conflict = {
  constraint: Education_Constraint
  update_columns?: Array<Education_Update_Column>
  where?: InputMaybe<Education_Bool_Exp>
}

/** Ordering options when selecting data from "education". */
export type Education_Order_By = {
  createdAt?: InputMaybe<Order_By>
  degree?: InputMaybe<Order_By>
  description?: InputMaybe<Order_By>
  fieldOfStudy?: InputMaybe<Order_By>
  from?: InputMaybe<Order_By>
  id?: InputMaybe<Order_By>
  isDeleted?: InputMaybe<Order_By>
  school?: InputMaybe<Order_By>
  to?: InputMaybe<Order_By>
  updatedAt?: InputMaybe<Order_By>
}

/** primary key columns input for table: education */
export type Education_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "education" */
export enum Education_Select_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Degree = 'degree',
  /** column name */
  Description = 'description',
  /** column name */
  FieldOfStudy = 'fieldOfStudy',
  /** column name */
  From = 'from',
  /** column name */
  Id = 'id',
  /** column name */
  IsDeleted = 'isDeleted',
  /** column name */
  School = 'school',
  /** column name */
  To = 'to',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** input type for updating data in table "education" */
export type Education_Set_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>
  degree?: InputMaybe<Scalars['String']>
  description?: InputMaybe<Scalars['String']>
  fieldOfStudy?: InputMaybe<Scalars['String']>
  from?: InputMaybe<Scalars['date']>
  id?: InputMaybe<Scalars['uuid']>
  isDeleted?: InputMaybe<Scalars['Boolean']>
  school?: InputMaybe<Scalars['String']>
  to?: InputMaybe<Scalars['date']>
  updatedAt?: InputMaybe<Scalars['timestamptz']>
}

/** update columns of table "education" */
export enum Education_Update_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Degree = 'degree',
  /** column name */
  Description = 'description',
  /** column name */
  FieldOfStudy = 'fieldOfStudy',
  /** column name */
  From = 'from',
  /** column name */
  Id = 'id',
  /** column name */
  IsDeleted = 'isDeleted',
  /** column name */
  School = 'school',
  /** column name */
  To = 'to',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** columns and relationships of "experience" */
export type Experience = {
  __typename?: 'experience'
  createdAt: Scalars['timestamptz']
  firmName?: Maybe<Scalars['String']>
  from?: Maybe<Scalars['date']>
  id: Scalars['uuid']
  isDeleted: Scalars['Boolean']
  name?: Maybe<Scalars['String']>
  note?: Maybe<Scalars['String']>
  position?: Maybe<Scalars['String']>
  to?: Maybe<Scalars['date']>
  type?: Maybe<Scalars['String']>
  updatedAt: Scalars['timestamptz']
}

/** aggregated selection of "experience" */
export type Experience_Aggregate = {
  __typename?: 'experience_aggregate'
  aggregate?: Maybe<Experience_Aggregate_Fields>
  nodes: Array<Experience>
}

/** aggregate fields of "experience" */
export type Experience_Aggregate_Fields = {
  __typename?: 'experience_aggregate_fields'
  count: Scalars['Int']
  max?: Maybe<Experience_Max_Fields>
  min?: Maybe<Experience_Min_Fields>
}

/** aggregate fields of "experience" */
export type Experience_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Experience_Select_Column>>
  distinct?: InputMaybe<Scalars['Boolean']>
}

/** Boolean expression to filter rows from the table "experience". All fields are combined with a logical 'AND'. */
export type Experience_Bool_Exp = {
  _and?: InputMaybe<Array<Experience_Bool_Exp>>
  _not?: InputMaybe<Experience_Bool_Exp>
  _or?: InputMaybe<Array<Experience_Bool_Exp>>
  createdAt?: InputMaybe<Timestamptz_Comparison_Exp>
  firmName?: InputMaybe<String_Comparison_Exp>
  from?: InputMaybe<Date_Comparison_Exp>
  id?: InputMaybe<Uuid_Comparison_Exp>
  isDeleted?: InputMaybe<Boolean_Comparison_Exp>
  name?: InputMaybe<String_Comparison_Exp>
  note?: InputMaybe<String_Comparison_Exp>
  position?: InputMaybe<String_Comparison_Exp>
  to?: InputMaybe<Date_Comparison_Exp>
  type?: InputMaybe<String_Comparison_Exp>
  updatedAt?: InputMaybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "experience" */
export enum Experience_Constraint {
  /** unique or primary key constraint on columns "id" */
  ExperiencePkey = 'experience_pkey'
}

/** input type for inserting data into table "experience" */
export type Experience_Insert_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>
  firmName?: InputMaybe<Scalars['String']>
  from?: InputMaybe<Scalars['date']>
  id?: InputMaybe<Scalars['uuid']>
  isDeleted?: InputMaybe<Scalars['Boolean']>
  name?: InputMaybe<Scalars['String']>
  note?: InputMaybe<Scalars['String']>
  position?: InputMaybe<Scalars['String']>
  to?: InputMaybe<Scalars['date']>
  type?: InputMaybe<Scalars['String']>
  updatedAt?: InputMaybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Experience_Max_Fields = {
  __typename?: 'experience_max_fields'
  createdAt?: Maybe<Scalars['timestamptz']>
  firmName?: Maybe<Scalars['String']>
  from?: Maybe<Scalars['date']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  note?: Maybe<Scalars['String']>
  position?: Maybe<Scalars['String']>
  to?: Maybe<Scalars['date']>
  type?: Maybe<Scalars['String']>
  updatedAt?: Maybe<Scalars['timestamptz']>
}

/** aggregate min on columns */
export type Experience_Min_Fields = {
  __typename?: 'experience_min_fields'
  createdAt?: Maybe<Scalars['timestamptz']>
  firmName?: Maybe<Scalars['String']>
  from?: Maybe<Scalars['date']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  note?: Maybe<Scalars['String']>
  position?: Maybe<Scalars['String']>
  to?: Maybe<Scalars['date']>
  type?: Maybe<Scalars['String']>
  updatedAt?: Maybe<Scalars['timestamptz']>
}

/** response of any mutation on the table "experience" */
export type Experience_Mutation_Response = {
  __typename?: 'experience_mutation_response'
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']
  /** data from the rows affected by the mutation */
  returning: Array<Experience>
}

/** on_conflict condition type for table "experience" */
export type Experience_On_Conflict = {
  constraint: Experience_Constraint
  update_columns?: Array<Experience_Update_Column>
  where?: InputMaybe<Experience_Bool_Exp>
}

/** Ordering options when selecting data from "experience". */
export type Experience_Order_By = {
  createdAt?: InputMaybe<Order_By>
  firmName?: InputMaybe<Order_By>
  from?: InputMaybe<Order_By>
  id?: InputMaybe<Order_By>
  isDeleted?: InputMaybe<Order_By>
  name?: InputMaybe<Order_By>
  note?: InputMaybe<Order_By>
  position?: InputMaybe<Order_By>
  to?: InputMaybe<Order_By>
  type?: InputMaybe<Order_By>
  updatedAt?: InputMaybe<Order_By>
}

/** primary key columns input for table: experience */
export type Experience_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "experience" */
export enum Experience_Select_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  FirmName = 'firmName',
  /** column name */
  From = 'from',
  /** column name */
  Id = 'id',
  /** column name */
  IsDeleted = 'isDeleted',
  /** column name */
  Name = 'name',
  /** column name */
  Note = 'note',
  /** column name */
  Position = 'position',
  /** column name */
  To = 'to',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** input type for updating data in table "experience" */
export type Experience_Set_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>
  firmName?: InputMaybe<Scalars['String']>
  from?: InputMaybe<Scalars['date']>
  id?: InputMaybe<Scalars['uuid']>
  isDeleted?: InputMaybe<Scalars['Boolean']>
  name?: InputMaybe<Scalars['String']>
  note?: InputMaybe<Scalars['String']>
  position?: InputMaybe<Scalars['String']>
  to?: InputMaybe<Scalars['date']>
  type?: InputMaybe<Scalars['String']>
  updatedAt?: InputMaybe<Scalars['timestamptz']>
}

/** update columns of table "experience" */
export enum Experience_Update_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  FirmName = 'firmName',
  /** column name */
  From = 'from',
  /** column name */
  Id = 'id',
  /** column name */
  IsDeleted = 'isDeleted',
  /** column name */
  Name = 'name',
  /** column name */
  Note = 'note',
  /** column name */
  Position = 'position',
  /** column name */
  To = 'to',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** mutation root */
export type Mutation_Root = {
  __typename?: 'mutation_root'
  /** delete data from the table: "education" */
  deleteEducationList?: Maybe<Education_Mutation_Response>
  /** delete single row from the table: "education" */
  deleteEducationOne?: Maybe<Education>
  /** delete data from the table: "experience" */
  deleteExperienceList?: Maybe<Experience_Mutation_Response>
  /** delete single row from the table: "experience" */
  deleteExperienceOne?: Maybe<Experience>
  /** delete single row from the table: "projects" */
  deleteProject?: Maybe<Project>
  /** delete data from the table: "projects" */
  deleteProjects?: Maybe<Project_Mutation_Response>
  /** delete single row from the table: "skills" */
  deleteSkill?: Maybe<Skill>
  /** delete single row from the table: "skill_groups" */
  deleteSkillGroup?: Maybe<SkillGroup>
  /** delete data from the table: "skill_groups" */
  deleteSkillGroups?: Maybe<SkillGroup_Mutation_Response>
  /** delete data from the table: "skills" */
  deleteSkills?: Maybe<Skill_Mutation_Response>
  /** insert data into the table: "education" */
  insertEducationList?: Maybe<Education_Mutation_Response>
  /** insert a single row into the table: "education" */
  insertEducationOne?: Maybe<Education>
  /** insert data into the table: "experience" */
  insertExperienceList?: Maybe<Experience_Mutation_Response>
  /** insert a single row into the table: "experience" */
  insertExperienceOne?: Maybe<Experience>
  /** insert a single row into the table: "projects" */
  insertProject?: Maybe<Project>
  /** insert data into the table: "projects" */
  insertProjects?: Maybe<Project_Mutation_Response>
  /** insert a single row into the table: "skills" */
  insertSkill?: Maybe<Skill>
  /** insert a single row into the table: "skill_groups" */
  insertSkillGroup?: Maybe<SkillGroup>
  /** insert data into the table: "skill_groups" */
  insertSkillGroups?: Maybe<SkillGroup_Mutation_Response>
  /** insert data into the table: "skills" */
  insertSkills?: Maybe<Skill_Mutation_Response>
  /** update data of the table: "education" */
  updateEducationList?: Maybe<Education_Mutation_Response>
  /** update single row of the table: "education" */
  updateEducationOne?: Maybe<Education>
  /** update data of the table: "experience" */
  updateExperienceList?: Maybe<Experience_Mutation_Response>
  /** update single row of the table: "experience" */
  updateExperienceOne?: Maybe<Experience>
  /** update single row of the table: "projects" */
  updateProject?: Maybe<Project>
  /** update data of the table: "projects" */
  updateProjects?: Maybe<Project_Mutation_Response>
  /** update single row of the table: "skills" */
  updateSkill?: Maybe<Skill>
  /** update single row of the table: "skill_groups" */
  updateSkillGroup?: Maybe<SkillGroup>
  /** update data of the table: "skill_groups" */
  updateSkillGroups?: Maybe<SkillGroup_Mutation_Response>
  /** update data of the table: "skills" */
  updateSkills?: Maybe<Skill_Mutation_Response>
}

/** mutation root */
export type Mutation_RootDeleteEducationListArgs = {
  where: Education_Bool_Exp
}

/** mutation root */
export type Mutation_RootDeleteEducationOneArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDeleteExperienceListArgs = {
  where: Experience_Bool_Exp
}

/** mutation root */
export type Mutation_RootDeleteExperienceOneArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDeleteProjectArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDeleteProjectsArgs = {
  where: Project_Bool_Exp
}

/** mutation root */
export type Mutation_RootDeleteSkillArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDeleteSkillGroupArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDeleteSkillGroupsArgs = {
  where: SkillGroup_Bool_Exp
}

/** mutation root */
export type Mutation_RootDeleteSkillsArgs = {
  where: Skill_Bool_Exp
}

/** mutation root */
export type Mutation_RootInsertEducationListArgs = {
  objects: Array<Education_Insert_Input>
  on_conflict?: InputMaybe<Education_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsertEducationOneArgs = {
  object: Education_Insert_Input
  on_conflict?: InputMaybe<Education_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsertExperienceListArgs = {
  objects: Array<Experience_Insert_Input>
  on_conflict?: InputMaybe<Experience_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsertExperienceOneArgs = {
  object: Experience_Insert_Input
  on_conflict?: InputMaybe<Experience_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsertProjectArgs = {
  object: Project_Insert_Input
  on_conflict?: InputMaybe<Project_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsertProjectsArgs = {
  objects: Array<Project_Insert_Input>
  on_conflict?: InputMaybe<Project_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsertSkillArgs = {
  object: Skill_Insert_Input
  on_conflict?: InputMaybe<Skill_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsertSkillGroupArgs = {
  object: SkillGroup_Insert_Input
  on_conflict?: InputMaybe<SkillGroup_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsertSkillGroupsArgs = {
  objects: Array<SkillGroup_Insert_Input>
  on_conflict?: InputMaybe<SkillGroup_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsertSkillsArgs = {
  objects: Array<Skill_Insert_Input>
  on_conflict?: InputMaybe<Skill_On_Conflict>
}

/** mutation root */
export type Mutation_RootUpdateEducationListArgs = {
  _set?: InputMaybe<Education_Set_Input>
  where: Education_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdateEducationOneArgs = {
  _set?: InputMaybe<Education_Set_Input>
  pk_columns: Education_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdateExperienceListArgs = {
  _set?: InputMaybe<Experience_Set_Input>
  where: Experience_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdateExperienceOneArgs = {
  _set?: InputMaybe<Experience_Set_Input>
  pk_columns: Experience_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdateProjectArgs = {
  _set?: InputMaybe<Project_Set_Input>
  pk_columns: Project_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdateProjectsArgs = {
  _set?: InputMaybe<Project_Set_Input>
  where: Project_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdateSkillArgs = {
  _set?: InputMaybe<Skill_Set_Input>
  pk_columns: Skill_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdateSkillGroupArgs = {
  _set?: InputMaybe<SkillGroup_Set_Input>
  pk_columns: SkillGroup_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdateSkillGroupsArgs = {
  _set?: InputMaybe<SkillGroup_Set_Input>
  where: SkillGroup_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdateSkillsArgs = {
  _set?: InputMaybe<Skill_Set_Input>
  where: Skill_Bool_Exp
}

/** column ordering options */
export enum Order_By {
  /** in ascending order, nulls last */
  Asc = 'asc',
  /** in ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in descending order, nulls first */
  Desc = 'desc',
  /** in descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** columns and relationships of "projects" */
export type Project = {
  __typename?: 'project'
  createdAt: Scalars['timestamptz']
  description?: Maybe<Scalars['String']>
  githubUrl?: Maybe<Scalars['String']>
  id: Scalars['uuid']
  imageUrl?: Maybe<Scalars['String']>
  isDeleted?: Maybe<Scalars['Boolean']>
  name: Scalars['String']
  /** updatedAt */
  updated_at: Scalars['timestamptz']
}

/** aggregated selection of "projects" */
export type Project_Aggregate = {
  __typename?: 'project_aggregate'
  aggregate?: Maybe<Project_Aggregate_Fields>
  nodes: Array<Project>
}

/** aggregate fields of "projects" */
export type Project_Aggregate_Fields = {
  __typename?: 'project_aggregate_fields'
  count: Scalars['Int']
  max?: Maybe<Project_Max_Fields>
  min?: Maybe<Project_Min_Fields>
}

/** aggregate fields of "projects" */
export type Project_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Project_Select_Column>>
  distinct?: InputMaybe<Scalars['Boolean']>
}

/** Boolean expression to filter rows from the table "projects". All fields are combined with a logical 'AND'. */
export type Project_Bool_Exp = {
  _and?: InputMaybe<Array<Project_Bool_Exp>>
  _not?: InputMaybe<Project_Bool_Exp>
  _or?: InputMaybe<Array<Project_Bool_Exp>>
  createdAt?: InputMaybe<Timestamptz_Comparison_Exp>
  description?: InputMaybe<String_Comparison_Exp>
  githubUrl?: InputMaybe<String_Comparison_Exp>
  id?: InputMaybe<Uuid_Comparison_Exp>
  imageUrl?: InputMaybe<String_Comparison_Exp>
  isDeleted?: InputMaybe<Boolean_Comparison_Exp>
  name?: InputMaybe<String_Comparison_Exp>
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "projects" */
export enum Project_Constraint {
  /** unique or primary key constraint on columns "id" */
  ProjectsPkey = 'projects_pkey'
}

/** input type for inserting data into table "projects" */
export type Project_Insert_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>
  description?: InputMaybe<Scalars['String']>
  githubUrl?: InputMaybe<Scalars['String']>
  id?: InputMaybe<Scalars['uuid']>
  imageUrl?: InputMaybe<Scalars['String']>
  isDeleted?: InputMaybe<Scalars['Boolean']>
  name?: InputMaybe<Scalars['String']>
  /** updatedAt */
  updated_at?: InputMaybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Project_Max_Fields = {
  __typename?: 'project_max_fields'
  createdAt?: Maybe<Scalars['timestamptz']>
  description?: Maybe<Scalars['String']>
  githubUrl?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  imageUrl?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
  /** updatedAt */
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate min on columns */
export type Project_Min_Fields = {
  __typename?: 'project_min_fields'
  createdAt?: Maybe<Scalars['timestamptz']>
  description?: Maybe<Scalars['String']>
  githubUrl?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  imageUrl?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
  /** updatedAt */
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** response of any mutation on the table "projects" */
export type Project_Mutation_Response = {
  __typename?: 'project_mutation_response'
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']
  /** data from the rows affected by the mutation */
  returning: Array<Project>
}

/** on_conflict condition type for table "projects" */
export type Project_On_Conflict = {
  constraint: Project_Constraint
  update_columns?: Array<Project_Update_Column>
  where?: InputMaybe<Project_Bool_Exp>
}

/** Ordering options when selecting data from "projects". */
export type Project_Order_By = {
  createdAt?: InputMaybe<Order_By>
  description?: InputMaybe<Order_By>
  githubUrl?: InputMaybe<Order_By>
  id?: InputMaybe<Order_By>
  imageUrl?: InputMaybe<Order_By>
  isDeleted?: InputMaybe<Order_By>
  name?: InputMaybe<Order_By>
  updated_at?: InputMaybe<Order_By>
}

/** primary key columns input for table: project */
export type Project_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "projects" */
export enum Project_Select_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Description = 'description',
  /** column name */
  GithubUrl = 'githubUrl',
  /** column name */
  Id = 'id',
  /** column name */
  ImageUrl = 'imageUrl',
  /** column name */
  IsDeleted = 'isDeleted',
  /** column name */
  Name = 'name',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "projects" */
export type Project_Set_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>
  description?: InputMaybe<Scalars['String']>
  githubUrl?: InputMaybe<Scalars['String']>
  id?: InputMaybe<Scalars['uuid']>
  imageUrl?: InputMaybe<Scalars['String']>
  isDeleted?: InputMaybe<Scalars['Boolean']>
  name?: InputMaybe<Scalars['String']>
  /** updatedAt */
  updated_at?: InputMaybe<Scalars['timestamptz']>
}

/** update columns of table "projects" */
export enum Project_Update_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Description = 'description',
  /** column name */
  GithubUrl = 'githubUrl',
  /** column name */
  Id = 'id',
  /** column name */
  ImageUrl = 'imageUrl',
  /** column name */
  IsDeleted = 'isDeleted',
  /** column name */
  Name = 'name',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Query_Root = {
  __typename?: 'query_root'
  /** fetch aggregated fields from the table: "education" */
  educationAggregate: Education_Aggregate
  /** fetch data from the table: "education" */
  educationList: Array<Education>
  /** fetch data from the table: "education" using primary key columns */
  educationOne?: Maybe<Education>
  /** fetch aggregated fields from the table: "experience" */
  experienceAggregate: Experience_Aggregate
  /** fetch data from the table: "experience" */
  experienceList: Array<Experience>
  /** fetch data from the table: "experience" using primary key columns */
  experienceOne?: Maybe<Experience>
  /** fetch data from the table: "projects" using primary key columns */
  project?: Maybe<Project>
  /** fetch aggregated fields from the table: "projects" */
  projectAggregate: Project_Aggregate
  /** fetch data from the table: "projects" */
  projects: Array<Project>
  /** fetch data from the table: "skills" using primary key columns */
  skill?: Maybe<Skill>
  /** fetch aggregated fields from the table: "skills" */
  skillAggregate: Skill_Aggregate
  /** fetch data from the table: "skill_groups" using primary key columns */
  skillGroup?: Maybe<SkillGroup>
  /** fetch aggregated fields from the table: "skill_groups" */
  skillGroupAggregate: SkillGroup_Aggregate
  /** fetch data from the table: "skill_groups" */
  skillGroups: Array<SkillGroup>
  /** An array relationship */
  skills: Array<Skill>
}

export type Query_RootEducationAggregateArgs = {
  distinct_on?: InputMaybe<Array<Education_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Education_Order_By>>
  where?: InputMaybe<Education_Bool_Exp>
}

export type Query_RootEducationListArgs = {
  distinct_on?: InputMaybe<Array<Education_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Education_Order_By>>
  where?: InputMaybe<Education_Bool_Exp>
}

export type Query_RootEducationOneArgs = {
  id: Scalars['uuid']
}

export type Query_RootExperienceAggregateArgs = {
  distinct_on?: InputMaybe<Array<Experience_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Experience_Order_By>>
  where?: InputMaybe<Experience_Bool_Exp>
}

export type Query_RootExperienceListArgs = {
  distinct_on?: InputMaybe<Array<Experience_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Experience_Order_By>>
  where?: InputMaybe<Experience_Bool_Exp>
}

export type Query_RootExperienceOneArgs = {
  id: Scalars['uuid']
}

export type Query_RootProjectArgs = {
  id: Scalars['uuid']
}

export type Query_RootProjectAggregateArgs = {
  distinct_on?: InputMaybe<Array<Project_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Project_Order_By>>
  where?: InputMaybe<Project_Bool_Exp>
}

export type Query_RootProjectsArgs = {
  distinct_on?: InputMaybe<Array<Project_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Project_Order_By>>
  where?: InputMaybe<Project_Bool_Exp>
}

export type Query_RootSkillArgs = {
  id: Scalars['uuid']
}

export type Query_RootSkillAggregateArgs = {
  distinct_on?: InputMaybe<Array<Skill_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Skill_Order_By>>
  where?: InputMaybe<Skill_Bool_Exp>
}

export type Query_RootSkillGroupArgs = {
  id: Scalars['uuid']
}

export type Query_RootSkillGroupAggregateArgs = {
  distinct_on?: InputMaybe<Array<SkillGroup_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<SkillGroup_Order_By>>
  where?: InputMaybe<SkillGroup_Bool_Exp>
}

export type Query_RootSkillGroupsArgs = {
  distinct_on?: InputMaybe<Array<SkillGroup_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<SkillGroup_Order_By>>
  where?: InputMaybe<SkillGroup_Bool_Exp>
}

export type Query_RootSkillsArgs = {
  distinct_on?: InputMaybe<Array<Skill_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Skill_Order_By>>
  where?: InputMaybe<Skill_Bool_Exp>
}

/** columns and relationships of "skills" */
export type Skill = {
  __typename?: 'skill'
  createdAt: Scalars['timestamptz']
  group?: Maybe<Scalars['String']>
  id: Scalars['uuid']
  isDeleted: Scalars['Boolean']
  level?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
  /** An object relationship */
  skillGroup?: Maybe<SkillGroup>
  skillGroupsId?: Maybe<Scalars['uuid']>
  updatedAt: Scalars['timestamptz']
}

/** columns and relationships of "skill_groups" */
export type SkillGroup = {
  __typename?: 'skillGroup'
  createdAt: Scalars['timestamptz']
  id: Scalars['uuid']
  isDeleted: Scalars['Boolean']
  name: Scalars['String']
  /** An array relationship */
  skills: Array<Skill>
  /** An aggregate relationship */
  skills_aggregate: Skill_Aggregate
  updatedAt: Scalars['timestamptz']
}

/** columns and relationships of "skill_groups" */
export type SkillGroupSkillsArgs = {
  distinct_on?: InputMaybe<Array<Skill_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Skill_Order_By>>
  where?: InputMaybe<Skill_Bool_Exp>
}

/** columns and relationships of "skill_groups" */
export type SkillGroupSkills_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Skill_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Skill_Order_By>>
  where?: InputMaybe<Skill_Bool_Exp>
}

/** aggregated selection of "skill_groups" */
export type SkillGroup_Aggregate = {
  __typename?: 'skillGroup_aggregate'
  aggregate?: Maybe<SkillGroup_Aggregate_Fields>
  nodes: Array<SkillGroup>
}

/** aggregate fields of "skill_groups" */
export type SkillGroup_Aggregate_Fields = {
  __typename?: 'skillGroup_aggregate_fields'
  count: Scalars['Int']
  max?: Maybe<SkillGroup_Max_Fields>
  min?: Maybe<SkillGroup_Min_Fields>
}

/** aggregate fields of "skill_groups" */
export type SkillGroup_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<SkillGroup_Select_Column>>
  distinct?: InputMaybe<Scalars['Boolean']>
}

/** Boolean expression to filter rows from the table "skill_groups". All fields are combined with a logical 'AND'. */
export type SkillGroup_Bool_Exp = {
  _and?: InputMaybe<Array<SkillGroup_Bool_Exp>>
  _not?: InputMaybe<SkillGroup_Bool_Exp>
  _or?: InputMaybe<Array<SkillGroup_Bool_Exp>>
  createdAt?: InputMaybe<Timestamptz_Comparison_Exp>
  id?: InputMaybe<Uuid_Comparison_Exp>
  isDeleted?: InputMaybe<Boolean_Comparison_Exp>
  name?: InputMaybe<String_Comparison_Exp>
  skills?: InputMaybe<Skill_Bool_Exp>
  updatedAt?: InputMaybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "skill_groups" */
export enum SkillGroup_Constraint {
  /** unique or primary key constraint on columns "id" */
  GroupsPkey = 'groups_pkey'
}

/** input type for inserting data into table "skill_groups" */
export type SkillGroup_Insert_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>
  id?: InputMaybe<Scalars['uuid']>
  isDeleted?: InputMaybe<Scalars['Boolean']>
  name?: InputMaybe<Scalars['String']>
  skills?: InputMaybe<Skill_Arr_Rel_Insert_Input>
  updatedAt?: InputMaybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type SkillGroup_Max_Fields = {
  __typename?: 'skillGroup_max_fields'
  createdAt?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  updatedAt?: Maybe<Scalars['timestamptz']>
}

/** aggregate min on columns */
export type SkillGroup_Min_Fields = {
  __typename?: 'skillGroup_min_fields'
  createdAt?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  updatedAt?: Maybe<Scalars['timestamptz']>
}

/** response of any mutation on the table "skill_groups" */
export type SkillGroup_Mutation_Response = {
  __typename?: 'skillGroup_mutation_response'
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']
  /** data from the rows affected by the mutation */
  returning: Array<SkillGroup>
}

/** input type for inserting object relation for remote table "skill_groups" */
export type SkillGroup_Obj_Rel_Insert_Input = {
  data: SkillGroup_Insert_Input
  /** upsert condition */
  on_conflict?: InputMaybe<SkillGroup_On_Conflict>
}

/** on_conflict condition type for table "skill_groups" */
export type SkillGroup_On_Conflict = {
  constraint: SkillGroup_Constraint
  update_columns?: Array<SkillGroup_Update_Column>
  where?: InputMaybe<SkillGroup_Bool_Exp>
}

/** Ordering options when selecting data from "skill_groups". */
export type SkillGroup_Order_By = {
  createdAt?: InputMaybe<Order_By>
  id?: InputMaybe<Order_By>
  isDeleted?: InputMaybe<Order_By>
  name?: InputMaybe<Order_By>
  skills_aggregate?: InputMaybe<Skill_Aggregate_Order_By>
  updatedAt?: InputMaybe<Order_By>
}

/** primary key columns input for table: skillGroup */
export type SkillGroup_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "skill_groups" */
export enum SkillGroup_Select_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Id = 'id',
  /** column name */
  IsDeleted = 'isDeleted',
  /** column name */
  Name = 'name',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** input type for updating data in table "skill_groups" */
export type SkillGroup_Set_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>
  id?: InputMaybe<Scalars['uuid']>
  isDeleted?: InputMaybe<Scalars['Boolean']>
  name?: InputMaybe<Scalars['String']>
  updatedAt?: InputMaybe<Scalars['timestamptz']>
}

/** update columns of table "skill_groups" */
export enum SkillGroup_Update_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Id = 'id',
  /** column name */
  IsDeleted = 'isDeleted',
  /** column name */
  Name = 'name',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** aggregated selection of "skills" */
export type Skill_Aggregate = {
  __typename?: 'skill_aggregate'
  aggregate?: Maybe<Skill_Aggregate_Fields>
  nodes: Array<Skill>
}

/** aggregate fields of "skills" */
export type Skill_Aggregate_Fields = {
  __typename?: 'skill_aggregate_fields'
  count: Scalars['Int']
  max?: Maybe<Skill_Max_Fields>
  min?: Maybe<Skill_Min_Fields>
}

/** aggregate fields of "skills" */
export type Skill_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Skill_Select_Column>>
  distinct?: InputMaybe<Scalars['Boolean']>
}

/** order by aggregate values of table "skills" */
export type Skill_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>
  max?: InputMaybe<Skill_Max_Order_By>
  min?: InputMaybe<Skill_Min_Order_By>
}

/** input type for inserting array relation for remote table "skills" */
export type Skill_Arr_Rel_Insert_Input = {
  data: Array<Skill_Insert_Input>
  /** upsert condition */
  on_conflict?: InputMaybe<Skill_On_Conflict>
}

/** Boolean expression to filter rows from the table "skills". All fields are combined with a logical 'AND'. */
export type Skill_Bool_Exp = {
  _and?: InputMaybe<Array<Skill_Bool_Exp>>
  _not?: InputMaybe<Skill_Bool_Exp>
  _or?: InputMaybe<Array<Skill_Bool_Exp>>
  createdAt?: InputMaybe<Timestamptz_Comparison_Exp>
  group?: InputMaybe<String_Comparison_Exp>
  id?: InputMaybe<Uuid_Comparison_Exp>
  isDeleted?: InputMaybe<Boolean_Comparison_Exp>
  level?: InputMaybe<String_Comparison_Exp>
  name?: InputMaybe<String_Comparison_Exp>
  skillGroup?: InputMaybe<SkillGroup_Bool_Exp>
  skillGroupsId?: InputMaybe<Uuid_Comparison_Exp>
  updatedAt?: InputMaybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "skills" */
export enum Skill_Constraint {
  /** unique or primary key constraint on columns "id" */
  SkillsPkey = 'skills_pkey'
}

/** input type for inserting data into table "skills" */
export type Skill_Insert_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>
  group?: InputMaybe<Scalars['String']>
  id?: InputMaybe<Scalars['uuid']>
  isDeleted?: InputMaybe<Scalars['Boolean']>
  level?: InputMaybe<Scalars['String']>
  name?: InputMaybe<Scalars['String']>
  skillGroup?: InputMaybe<SkillGroup_Obj_Rel_Insert_Input>
  skillGroupsId?: InputMaybe<Scalars['uuid']>
  updatedAt?: InputMaybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Skill_Max_Fields = {
  __typename?: 'skill_max_fields'
  createdAt?: Maybe<Scalars['timestamptz']>
  group?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  level?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
  skillGroupsId?: Maybe<Scalars['uuid']>
  updatedAt?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "skills" */
export type Skill_Max_Order_By = {
  createdAt?: InputMaybe<Order_By>
  group?: InputMaybe<Order_By>
  id?: InputMaybe<Order_By>
  level?: InputMaybe<Order_By>
  name?: InputMaybe<Order_By>
  skillGroupsId?: InputMaybe<Order_By>
  updatedAt?: InputMaybe<Order_By>
}

/** aggregate min on columns */
export type Skill_Min_Fields = {
  __typename?: 'skill_min_fields'
  createdAt?: Maybe<Scalars['timestamptz']>
  group?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  level?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
  skillGroupsId?: Maybe<Scalars['uuid']>
  updatedAt?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "skills" */
export type Skill_Min_Order_By = {
  createdAt?: InputMaybe<Order_By>
  group?: InputMaybe<Order_By>
  id?: InputMaybe<Order_By>
  level?: InputMaybe<Order_By>
  name?: InputMaybe<Order_By>
  skillGroupsId?: InputMaybe<Order_By>
  updatedAt?: InputMaybe<Order_By>
}

/** response of any mutation on the table "skills" */
export type Skill_Mutation_Response = {
  __typename?: 'skill_mutation_response'
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']
  /** data from the rows affected by the mutation */
  returning: Array<Skill>
}

/** on_conflict condition type for table "skills" */
export type Skill_On_Conflict = {
  constraint: Skill_Constraint
  update_columns?: Array<Skill_Update_Column>
  where?: InputMaybe<Skill_Bool_Exp>
}

/** Ordering options when selecting data from "skills". */
export type Skill_Order_By = {
  createdAt?: InputMaybe<Order_By>
  group?: InputMaybe<Order_By>
  id?: InputMaybe<Order_By>
  isDeleted?: InputMaybe<Order_By>
  level?: InputMaybe<Order_By>
  name?: InputMaybe<Order_By>
  skillGroup?: InputMaybe<SkillGroup_Order_By>
  skillGroupsId?: InputMaybe<Order_By>
  updatedAt?: InputMaybe<Order_By>
}

/** primary key columns input for table: skill */
export type Skill_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "skills" */
export enum Skill_Select_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Group = 'group',
  /** column name */
  Id = 'id',
  /** column name */
  IsDeleted = 'isDeleted',
  /** column name */
  Level = 'level',
  /** column name */
  Name = 'name',
  /** column name */
  SkillGroupsId = 'skillGroupsId',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** input type for updating data in table "skills" */
export type Skill_Set_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>
  group?: InputMaybe<Scalars['String']>
  id?: InputMaybe<Scalars['uuid']>
  isDeleted?: InputMaybe<Scalars['Boolean']>
  level?: InputMaybe<Scalars['String']>
  name?: InputMaybe<Scalars['String']>
  skillGroupsId?: InputMaybe<Scalars['uuid']>
  updatedAt?: InputMaybe<Scalars['timestamptz']>
}

/** update columns of table "skills" */
export enum Skill_Update_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Group = 'group',
  /** column name */
  Id = 'id',
  /** column name */
  IsDeleted = 'isDeleted',
  /** column name */
  Level = 'level',
  /** column name */
  Name = 'name',
  /** column name */
  SkillGroupsId = 'skillGroupsId',
  /** column name */
  UpdatedAt = 'updatedAt'
}

export type Subscription_Root = {
  __typename?: 'subscription_root'
  /** fetch aggregated fields from the table: "education" */
  educationAggregate: Education_Aggregate
  /** fetch data from the table: "education" */
  educationList: Array<Education>
  /** fetch data from the table: "education" using primary key columns */
  educationOne?: Maybe<Education>
  /** fetch aggregated fields from the table: "experience" */
  experienceAggregate: Experience_Aggregate
  /** fetch data from the table: "experience" */
  experienceList: Array<Experience>
  /** fetch data from the table: "experience" using primary key columns */
  experienceOne?: Maybe<Experience>
  /** fetch data from the table: "projects" using primary key columns */
  project?: Maybe<Project>
  /** fetch aggregated fields from the table: "projects" */
  projectAggregate: Project_Aggregate
  /** fetch data from the table: "projects" */
  projects: Array<Project>
  /** fetch data from the table: "skills" using primary key columns */
  skill?: Maybe<Skill>
  /** fetch aggregated fields from the table: "skills" */
  skillAggregate: Skill_Aggregate
  /** fetch data from the table: "skill_groups" using primary key columns */
  skillGroup?: Maybe<SkillGroup>
  /** fetch aggregated fields from the table: "skill_groups" */
  skillGroupAggregate: SkillGroup_Aggregate
  /** fetch data from the table: "skill_groups" */
  skillGroups: Array<SkillGroup>
  /** An array relationship */
  skills: Array<Skill>
}

export type Subscription_RootEducationAggregateArgs = {
  distinct_on?: InputMaybe<Array<Education_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Education_Order_By>>
  where?: InputMaybe<Education_Bool_Exp>
}

export type Subscription_RootEducationListArgs = {
  distinct_on?: InputMaybe<Array<Education_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Education_Order_By>>
  where?: InputMaybe<Education_Bool_Exp>
}

export type Subscription_RootEducationOneArgs = {
  id: Scalars['uuid']
}

export type Subscription_RootExperienceAggregateArgs = {
  distinct_on?: InputMaybe<Array<Experience_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Experience_Order_By>>
  where?: InputMaybe<Experience_Bool_Exp>
}

export type Subscription_RootExperienceListArgs = {
  distinct_on?: InputMaybe<Array<Experience_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Experience_Order_By>>
  where?: InputMaybe<Experience_Bool_Exp>
}

export type Subscription_RootExperienceOneArgs = {
  id: Scalars['uuid']
}

export type Subscription_RootProjectArgs = {
  id: Scalars['uuid']
}

export type Subscription_RootProjectAggregateArgs = {
  distinct_on?: InputMaybe<Array<Project_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Project_Order_By>>
  where?: InputMaybe<Project_Bool_Exp>
}

export type Subscription_RootProjectsArgs = {
  distinct_on?: InputMaybe<Array<Project_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Project_Order_By>>
  where?: InputMaybe<Project_Bool_Exp>
}

export type Subscription_RootSkillArgs = {
  id: Scalars['uuid']
}

export type Subscription_RootSkillAggregateArgs = {
  distinct_on?: InputMaybe<Array<Skill_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Skill_Order_By>>
  where?: InputMaybe<Skill_Bool_Exp>
}

export type Subscription_RootSkillGroupArgs = {
  id: Scalars['uuid']
}

export type Subscription_RootSkillGroupAggregateArgs = {
  distinct_on?: InputMaybe<Array<SkillGroup_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<SkillGroup_Order_By>>
  where?: InputMaybe<SkillGroup_Bool_Exp>
}

export type Subscription_RootSkillGroupsArgs = {
  distinct_on?: InputMaybe<Array<SkillGroup_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<SkillGroup_Order_By>>
  where?: InputMaybe<SkillGroup_Bool_Exp>
}

export type Subscription_RootSkillsArgs = {
  distinct_on?: InputMaybe<Array<Skill_Select_Column>>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Skill_Order_By>>
  where?: InputMaybe<Skill_Bool_Exp>
}

export type Timestamptz_Cast_Exp = {
  String?: InputMaybe<String_Comparison_Exp>
}

/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _cast?: InputMaybe<Timestamptz_Cast_Exp>
  _eq?: InputMaybe<Scalars['timestamptz']>
  _gt?: InputMaybe<Scalars['timestamptz']>
  _gte?: InputMaybe<Scalars['timestamptz']>
  _in?: InputMaybe<Array<Scalars['timestamptz']>>
  _is_null?: InputMaybe<Scalars['Boolean']>
  _lt?: InputMaybe<Scalars['timestamptz']>
  _lte?: InputMaybe<Scalars['timestamptz']>
  _neq?: InputMaybe<Scalars['timestamptz']>
  _nin?: InputMaybe<Array<Scalars['timestamptz']>>
}

export type Uuid_Cast_Exp = {
  String?: InputMaybe<String_Comparison_Exp>
}

/** Boolean expression to compare columns of type "uuid". All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
  _cast?: InputMaybe<Uuid_Cast_Exp>
  _eq?: InputMaybe<Scalars['uuid']>
  _gt?: InputMaybe<Scalars['uuid']>
  _gte?: InputMaybe<Scalars['uuid']>
  _in?: InputMaybe<Array<Scalars['uuid']>>
  _is_null?: InputMaybe<Scalars['Boolean']>
  _lt?: InputMaybe<Scalars['uuid']>
  _lte?: InputMaybe<Scalars['uuid']>
  _neq?: InputMaybe<Scalars['uuid']>
  _nin?: InputMaybe<Array<Scalars['uuid']>>
}

export type EducationListQueryVariables = Exact<{
  distinct_on?: InputMaybe<
    Array<Education_Select_Column> | Education_Select_Column
  >
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Education_Order_By> | Education_Order_By>
  where?: InputMaybe<Education_Bool_Exp>
}>

export type EducationListQuery = {
  __typename?: 'query_root'
  educationList: Array<{
    __typename?: 'education'
    id: any
    isDeleted: boolean
    from: any
    to?: any | null
    degree?: string | null
    fieldOfStudy?: string | null
    school?: string | null
  }>
}

export type EducationOneQueryVariables = Exact<{
  id: Scalars['uuid']
}>

export type EducationOneQuery = {
  __typename?: 'query_root'
  educationOne?: {
    __typename?: 'education'
    id: any
    isDeleted: boolean
    from: any
    to?: any | null
    degree?: string | null
    description?: string | null
    fieldOfStudy?: string | null
    school?: string | null
  } | null
}

export type InsertEducationOneMutationVariables = Exact<{
  education: Education_Insert_Input
  on_conflict?: InputMaybe<Education_On_Conflict>
}>

export type InsertEducationOneMutation = {
  __typename?: 'mutation_root'
  insertEducationOne?: {
    __typename?: 'education'
    id: any
    isDeleted: boolean
    from: any
    to?: any | null
    degree?: string | null
    description?: string | null
    fieldOfStudy?: string | null
    school?: string | null
  } | null
}

export type InsertEducationListMutationVariables = Exact<{
  education: Array<Education_Insert_Input> | Education_Insert_Input
  on_conflict?: InputMaybe<Education_On_Conflict>
}>

export type InsertEducationListMutation = {
  __typename?: 'mutation_root'
  insertEducationList?: {
    __typename?: 'education_mutation_response'
    returning: Array<{
      __typename?: 'education'
      id: any
      isDeleted: boolean
      from: any
      to?: any | null
      degree?: string | null
      description?: string | null
      fieldOfStudy?: string | null
      school?: string | null
    }>
  } | null
}

export type UpdateEducationOneMutationVariables = Exact<{
  education?: InputMaybe<Education_Set_Input>
  pk_columns: Education_Pk_Columns_Input
}>

export type UpdateEducationOneMutation = {
  __typename?: 'mutation_root'
  updateEducationOne?: {
    __typename?: 'education'
    id: any
    isDeleted: boolean
    from: any
    to?: any | null
    degree?: string | null
    description?: string | null
    fieldOfStudy?: string | null
    school?: string | null
  } | null
}

export type EducationOne_EducationOneFragment = {
  __typename?: 'education'
  id: any
  isDeleted: boolean
  from: any
  to?: any | null
  degree?: string | null
  description?: string | null
  fieldOfStudy?: string | null
  school?: string | null
}

export type EducationList_EducationListFragment = {
  __typename?: 'education'
  id: any
  isDeleted: boolean
  from: any
  to?: any | null
  degree?: string | null
  fieldOfStudy?: string | null
  school?: string | null
}

export type InsertEducationOne_EducationOneFragment = {
  __typename?: 'education'
  id: any
  isDeleted: boolean
  from: any
  to?: any | null
  degree?: string | null
  description?: string | null
  fieldOfStudy?: string | null
  school?: string | null
}

export type InsertEducationList_EducationLists_Mutation_ResponseFragment = {
  __typename?: 'education_mutation_response'
  returning: Array<{
    __typename?: 'education'
    id: any
    isDeleted: boolean
    from: any
    to?: any | null
    degree?: string | null
    description?: string | null
    fieldOfStudy?: string | null
    school?: string | null
  }>
}

export type UpdateEducationOne_EducationOneFragment = {
  __typename?: 'education'
  id: any
  isDeleted: boolean
  from: any
  to?: any | null
  degree?: string | null
  description?: string | null
  fieldOfStudy?: string | null
  school?: string | null
}

export type ExperienceListQueryVariables = Exact<{
  distinct_on?: InputMaybe<
    Array<Experience_Select_Column> | Experience_Select_Column
  >
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Experience_Order_By> | Experience_Order_By>
  where?: InputMaybe<Experience_Bool_Exp>
}>

export type ExperienceListQuery = {
  __typename?: 'query_root'
  experienceList: Array<{
    __typename?: 'experience'
    id: any
    isDeleted: boolean
    from?: any | null
    to?: any | null
    firmName?: string | null
    name?: string | null
    position?: string | null
    type?: string | null
  }>
}

export type ExperienceOneQueryVariables = Exact<{
  id: Scalars['uuid']
}>

export type ExperienceOneQuery = {
  __typename?: 'query_root'
  experienceOne?: {
    __typename?: 'experience'
    id: any
    isDeleted: boolean
    from?: any | null
    to?: any | null
    firmName?: string | null
    name?: string | null
    note?: string | null
    position?: string | null
    type?: string | null
  } | null
}

export type InsertExperienceOneMutationVariables = Exact<{
  experience: Experience_Insert_Input
  on_conflict?: InputMaybe<Experience_On_Conflict>
}>

export type InsertExperienceOneMutation = {
  __typename?: 'mutation_root'
  insertExperienceOne?: {
    __typename?: 'experience'
    id: any
    isDeleted: boolean
    from?: any | null
    to?: any | null
    firmName?: string | null
    name?: string | null
    position?: string | null
    type?: string | null
    note?: string | null
  } | null
}

export type InsertExperienceListMutationVariables = Exact<{
  experiences: Array<Experience_Insert_Input> | Experience_Insert_Input
  on_conflict?: InputMaybe<Experience_On_Conflict>
}>

export type InsertExperienceListMutation = {
  __typename?: 'mutation_root'
  insertExperienceList?: {
    __typename?: 'experience_mutation_response'
    returning: Array<{
      __typename?: 'experience'
      id: any
      isDeleted: boolean
      from?: any | null
      to?: any | null
      firmName?: string | null
      name?: string | null
      position?: string | null
      type?: string | null
      note?: string | null
    }>
  } | null
}

export type UpdateExperienceOneMutationVariables = Exact<{
  experience?: InputMaybe<Experience_Set_Input>
  pk_columns: Experience_Pk_Columns_Input
}>

export type UpdateExperienceOneMutation = {
  __typename?: 'mutation_root'
  updateExperienceOne?: {
    __typename?: 'experience'
    id: any
    isDeleted: boolean
    from?: any | null
    to?: any | null
    firmName?: string | null
    name?: string | null
    position?: string | null
    type?: string | null
    note?: string | null
  } | null
}

export type ExperienceOne_ExperienceOneFragment = {
  __typename?: 'experience'
  id: any
  isDeleted: boolean
  from?: any | null
  to?: any | null
  firmName?: string | null
  name?: string | null
  note?: string | null
  position?: string | null
  type?: string | null
}

export type ExperienceList_ExperienceListFragment = {
  __typename?: 'experience'
  id: any
  isDeleted: boolean
  from?: any | null
  to?: any | null
  firmName?: string | null
  name?: string | null
  position?: string | null
  type?: string | null
}

export type InsertExperienceOne_ExperienceOneFragment = {
  __typename?: 'experience'
  id: any
  isDeleted: boolean
  from?: any | null
  to?: any | null
  firmName?: string | null
  name?: string | null
  position?: string | null
  type?: string | null
  note?: string | null
}

export type InsertExperienceList_ExperienceLists_Mutation_ResponseFragment = {
  __typename?: 'experience_mutation_response'
  returning: Array<{
    __typename?: 'experience'
    id: any
    isDeleted: boolean
    from?: any | null
    to?: any | null
    firmName?: string | null
    name?: string | null
    position?: string | null
    type?: string | null
    note?: string | null
  }>
}

export type UpdateExperienceOne_ExperienceOneFragment = {
  __typename?: 'experience'
  id: any
  isDeleted: boolean
  from?: any | null
  to?: any | null
  firmName?: string | null
  name?: string | null
  position?: string | null
  type?: string | null
  note?: string | null
}

export type DummyQueryVariables = Exact<{ [key: string]: never }>

export type DummyQuery = { __typename: 'query_root' }

export type ProjectsQueryVariables = Exact<{
  distinct_on?: InputMaybe<Array<Project_Select_Column> | Project_Select_Column>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Project_Order_By> | Project_Order_By>
  where?: InputMaybe<Project_Bool_Exp>
}>

export type ProjectsQuery = {
  __typename?: 'query_root'
  projects: Array<{
    __typename?: 'project'
    id: any
    name: string
    isDeleted?: boolean | null
    githubUrl?: string | null
    description?: string | null
  }>
}

export type ProjectQueryVariables = Exact<{
  id: Scalars['uuid']
}>

export type ProjectQuery = {
  __typename?: 'query_root'
  project?: {
    __typename?: 'project'
    id: any
    isDeleted?: boolean | null
    description?: string | null
    githubUrl?: string | null
    imageUrl?: string | null
    name: string
  } | null
}

export type InsertProjectMutationVariables = Exact<{
  project: Project_Insert_Input
  on_conflict?: InputMaybe<Project_On_Conflict>
}>

export type InsertProjectMutation = {
  __typename?: 'mutation_root'
  insertProject?: {
    __typename?: 'project'
    id: any
    isDeleted?: boolean | null
    description?: string | null
    githubUrl?: string | null
    name: string
  } | null
}

export type UpdateProjectMutationVariables = Exact<{
  project?: InputMaybe<Project_Set_Input>
  pk_columns: Project_Pk_Columns_Input
}>

export type UpdateProjectMutation = {
  __typename?: 'mutation_root'
  updateProject?: {
    __typename?: 'project'
    id: any
    isDeleted?: boolean | null
    description?: string | null
    githubUrl?: string | null
    name: string
  } | null
}

export type Project_ProjectFragment = {
  __typename?: 'project'
  id: any
  isDeleted?: boolean | null
  description?: string | null
  githubUrl?: string | null
  imageUrl?: string | null
  name: string
}

export type Projects_ProjectsFragment = {
  __typename?: 'project'
  id: any
  name: string
  isDeleted?: boolean | null
  githubUrl?: string | null
  description?: string | null
}

export type InsertProject_ProjectFragment = {
  __typename?: 'project'
  id: any
  isDeleted?: boolean | null
  description?: string | null
  githubUrl?: string | null
  name: string
}

export type UpdateProject_ProjectFragment = {
  __typename?: 'project'
  id: any
  isDeleted?: boolean | null
  description?: string | null
  githubUrl?: string | null
  name: string
}

export type SkillGroupsQueryVariables = Exact<{
  distinct_on?: InputMaybe<
    Array<SkillGroup_Select_Column> | SkillGroup_Select_Column
  >
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<SkillGroup_Order_By> | SkillGroup_Order_By>
  where?: InputMaybe<SkillGroup_Bool_Exp>
}>

export type SkillGroupsQuery = {
  __typename?: 'query_root'
  skillGroups: Array<{
    __typename?: 'skillGroup'
    id: any
    isDeleted: boolean
    name: string
    skills: Array<{
      __typename?: 'skill'
      id: any
      isDeleted: boolean
      group?: string | null
      level?: string | null
      name?: string | null
    }>
  }>
}

export type Skill_GroupsFragment = {
  __typename?: 'skillGroup'
  id: any
  isDeleted: boolean
  name: string
}

export type SkillsGroup_SkillsFragment = {
  __typename?: 'skill'
  id: any
  isDeleted: boolean
  group?: string | null
  level?: string | null
  name?: string | null
}

export type SkillsQueryVariables = Exact<{
  distinct_on?: InputMaybe<Array<Skill_Select_Column> | Skill_Select_Column>
  limit?: InputMaybe<Scalars['Int']>
  offset?: InputMaybe<Scalars['Int']>
  order_by?: InputMaybe<Array<Skill_Order_By> | Skill_Order_By>
  where?: InputMaybe<Skill_Bool_Exp>
}>

export type SkillsQuery = {
  __typename?: 'query_root'
  skills: Array<{
    __typename?: 'skill'
    id: any
    isDeleted: boolean
    level?: string | null
    name?: string | null
  }>
}

export type SkillQueryVariables = Exact<{
  id: Scalars['uuid']
}>

export type SkillQuery = {
  __typename?: 'query_root'
  skill?: {
    __typename?: 'skill'
    id: any
    isDeleted: boolean
    level?: string | null
    name?: string | null
    skillGroupsId?: any | null
  } | null
}

export type InsertSkillMutationVariables = Exact<{
  skill: Skill_Insert_Input
  on_conflict?: InputMaybe<Skill_On_Conflict>
}>

export type InsertSkillMutation = {
  __typename?: 'mutation_root'
  insertSkill?: {
    __typename?: 'skill'
    id: any
    isDeleted: boolean
    level?: string | null
    name?: string | null
    skillGroupsId?: any | null
  } | null
}

export type UpdateSkillMutationVariables = Exact<{
  skill?: InputMaybe<Skill_Set_Input>
  pk_columns: Skill_Pk_Columns_Input
}>

export type UpdateSkillMutation = {
  __typename?: 'mutation_root'
  updateSkill?: {
    __typename?: 'skill'
    id: any
    isDeleted: boolean
    level?: string | null
    name?: string | null
    skillGroupsId?: any | null
  } | null
}

export type Skill_SkillsFragment = {
  __typename?: 'skill'
  id: any
  isDeleted: boolean
  level?: string | null
  name?: string | null
  skillGroupsId?: any | null
}

export type Skills_SkillsFragment = {
  __typename?: 'skill'
  id: any
  isDeleted: boolean
  level?: string | null
  name?: string | null
}

export type InsertSkill_SkillFragment = {
  __typename?: 'skill'
  id: any
  isDeleted: boolean
  level?: string | null
  name?: string | null
  skillGroupsId?: any | null
}

export type UpdateSkill_SkillFragment = {
  __typename?: 'skill'
  id: any
  isDeleted: boolean
  level?: string | null
  name?: string | null
  skillGroupsId?: any | null
}

export const EducationOne_EducationOneFragmentDoc = gql`
  fragment educationOne_educationOne on education {
    id
    isDeleted
    from
    to
    degree
    description
    fieldOfStudy
    school
  }
`
export const EducationList_EducationListFragmentDoc = gql`
  fragment educationList_educationList on education {
    id
    isDeleted
    from
    to
    degree
    fieldOfStudy
    school
  }
`
export const InsertEducationOne_EducationOneFragmentDoc = gql`
  fragment insertEducationOne_educationOne on education {
    id
    isDeleted
    from
    to
    degree
    description
    fieldOfStudy
    school
  }
`
export const InsertEducationList_EducationLists_Mutation_ResponseFragmentDoc = gql`
  fragment insertEducationList_educationLists_mutation_response on education_mutation_response {
    returning {
      id
      isDeleted
      from
      to
      degree
      description
      fieldOfStudy
      school
    }
  }
`
export const UpdateEducationOne_EducationOneFragmentDoc = gql`
  fragment updateEducationOne_educationOne on education {
    id
    isDeleted
    from
    to
    degree
    description
    fieldOfStudy
    school
  }
`
export const ExperienceOne_ExperienceOneFragmentDoc = gql`
  fragment experienceOne_experienceOne on experience {
    id
    isDeleted
    from
    to
    firmName
    name
    note
    position
    type
  }
`
export const ExperienceList_ExperienceListFragmentDoc = gql`
  fragment experienceList_experienceList on experience {
    id
    isDeleted
    from
    to
    firmName
    name
    position
    type
  }
`
export const InsertExperienceOne_ExperienceOneFragmentDoc = gql`
  fragment insertExperienceOne_experienceOne on experience {
    id
    isDeleted
    from
    to
    firmName
    name
    position
    type
    note
  }
`
export const InsertExperienceList_ExperienceLists_Mutation_ResponseFragmentDoc = gql`
  fragment insertExperienceList_experienceLists_mutation_response on experience_mutation_response {
    returning {
      id
      isDeleted
      from
      to
      firmName
      name
      position
      type
      note
    }
  }
`
export const UpdateExperienceOne_ExperienceOneFragmentDoc = gql`
  fragment updateExperienceOne_experienceOne on experience {
    id
    isDeleted
    from
    to
    firmName
    name
    position
    type
    note
  }
`
export const Project_ProjectFragmentDoc = gql`
  fragment project_project on project {
    id
    isDeleted
    description
    githubUrl
    imageUrl
    name
  }
`
export const Projects_ProjectsFragmentDoc = gql`
  fragment projects_projects on project {
    id
    name
    isDeleted
    githubUrl
    description
  }
`
export const InsertProject_ProjectFragmentDoc = gql`
  fragment insertProject_project on project {
    id
    isDeleted
    description
    githubUrl
    name
  }
`
export const UpdateProject_ProjectFragmentDoc = gql`
  fragment updateProject_project on project {
    id
    isDeleted
    description
    githubUrl
    name
  }
`
export const Skill_GroupsFragmentDoc = gql`
  fragment skill_groups on skillGroup {
    id
    isDeleted
    name
  }
`
export const SkillsGroup_SkillsFragmentDoc = gql`
  fragment skillsGroup_skills on skill {
    id
    isDeleted
    group
    level
    name
  }
`
export const Skill_SkillsFragmentDoc = gql`
  fragment skill_skills on skill {
    id
    isDeleted
    level
    name
    skillGroupsId
  }
`
export const Skills_SkillsFragmentDoc = gql`
  fragment skills_skills on skill {
    id
    isDeleted
    level
    name
  }
`
export const InsertSkill_SkillFragmentDoc = gql`
  fragment insertSkill_skill on skill {
    id
    isDeleted
    level
    name
    skillGroupsId
  }
`
export const UpdateSkill_SkillFragmentDoc = gql`
  fragment updateSkill_skill on skill {
    id
    isDeleted
    level
    name
    skillGroupsId
  }
`
export const EducationListDocument = gql`
  query educationList(
    $distinct_on: [education_select_column!]
    $limit: Int
    $offset: Int
    $order_by: [education_order_by!]
    $where: education_bool_exp
  ) {
    educationList(
      distinct_on: $distinct_on
      limit: $limit
      offset: $offset
      order_by: $order_by
      where: $where
    ) {
      ...educationList_educationList
    }
  }
  ${EducationList_EducationListFragmentDoc}
`

export function useEducationListQuery(
  options?: Omit<Urql.UseQueryArgs<EducationListQueryVariables>, 'query'>
) {
  return Urql.useQuery<EducationListQuery>({
    query: EducationListDocument,
    ...options
  })
}
export const EducationOneDocument = gql`
  query educationOne($id: uuid!) {
    educationOne(id: $id) {
      ...educationOne_educationOne
    }
  }
  ${EducationOne_EducationOneFragmentDoc}
`

export function useEducationOneQuery(
  options: Omit<Urql.UseQueryArgs<EducationOneQueryVariables>, 'query'>
) {
  return Urql.useQuery<EducationOneQuery>({
    query: EducationOneDocument,
    ...options
  })
}
export const InsertEducationOneDocument = gql`
  mutation insertEducationOne(
    $education: education_insert_input!
    $on_conflict: education_on_conflict
  ) {
    insertEducationOne(object: $education, on_conflict: $on_conflict) {
      ...insertEducationOne_educationOne
    }
  }
  ${InsertEducationOne_EducationOneFragmentDoc}
`

export function useInsertEducationOneMutation() {
  return Urql.useMutation<
    InsertEducationOneMutation,
    InsertEducationOneMutationVariables
  >(InsertEducationOneDocument)
}
export const InsertEducationListDocument = gql`
  mutation insertEducationList(
    $education: [education_insert_input!]!
    $on_conflict: education_on_conflict
  ) {
    insertEducationList(objects: $education, on_conflict: $on_conflict) {
      ...insertEducationList_educationLists_mutation_response
    }
  }
  ${InsertEducationList_EducationLists_Mutation_ResponseFragmentDoc}
`

export function useInsertEducationListMutation() {
  return Urql.useMutation<
    InsertEducationListMutation,
    InsertEducationListMutationVariables
  >(InsertEducationListDocument)
}
export const UpdateEducationOneDocument = gql`
  mutation updateEducationOne(
    $education: education_set_input
    $pk_columns: education_pk_columns_input!
  ) {
    updateEducationOne(_set: $education, pk_columns: $pk_columns) {
      ...updateEducationOne_educationOne
    }
  }
  ${UpdateEducationOne_EducationOneFragmentDoc}
`

export function useUpdateEducationOneMutation() {
  return Urql.useMutation<
    UpdateEducationOneMutation,
    UpdateEducationOneMutationVariables
  >(UpdateEducationOneDocument)
}
export const ExperienceListDocument = gql`
  query experienceList(
    $distinct_on: [experience_select_column!]
    $limit: Int
    $offset: Int
    $order_by: [experience_order_by!]
    $where: experience_bool_exp
  ) {
    experienceList(
      distinct_on: $distinct_on
      limit: $limit
      offset: $offset
      order_by: $order_by
      where: $where
    ) {
      ...experienceList_experienceList
    }
  }
  ${ExperienceList_ExperienceListFragmentDoc}
`

export function useExperienceListQuery(
  options?: Omit<Urql.UseQueryArgs<ExperienceListQueryVariables>, 'query'>
) {
  return Urql.useQuery<ExperienceListQuery>({
    query: ExperienceListDocument,
    ...options
  })
}
export const ExperienceOneDocument = gql`
  query experienceOne($id: uuid!) {
    experienceOne(id: $id) {
      ...experienceOne_experienceOne
    }
  }
  ${ExperienceOne_ExperienceOneFragmentDoc}
`

export function useExperienceOneQuery(
  options: Omit<Urql.UseQueryArgs<ExperienceOneQueryVariables>, 'query'>
) {
  return Urql.useQuery<ExperienceOneQuery>({
    query: ExperienceOneDocument,
    ...options
  })
}
export const InsertExperienceOneDocument = gql`
  mutation insertExperienceOne(
    $experience: experience_insert_input!
    $on_conflict: experience_on_conflict
  ) {
    insertExperienceOne(object: $experience, on_conflict: $on_conflict) {
      ...insertExperienceOne_experienceOne
    }
  }
  ${InsertExperienceOne_ExperienceOneFragmentDoc}
`

export function useInsertExperienceOneMutation() {
  return Urql.useMutation<
    InsertExperienceOneMutation,
    InsertExperienceOneMutationVariables
  >(InsertExperienceOneDocument)
}
export const InsertExperienceListDocument = gql`
  mutation insertExperienceList(
    $experiences: [experience_insert_input!]!
    $on_conflict: experience_on_conflict
  ) {
    insertExperienceList(objects: $experiences, on_conflict: $on_conflict) {
      ...insertExperienceList_experienceLists_mutation_response
    }
  }
  ${InsertExperienceList_ExperienceLists_Mutation_ResponseFragmentDoc}
`

export function useInsertExperienceListMutation() {
  return Urql.useMutation<
    InsertExperienceListMutation,
    InsertExperienceListMutationVariables
  >(InsertExperienceListDocument)
}
export const UpdateExperienceOneDocument = gql`
  mutation updateExperienceOne(
    $experience: experience_set_input
    $pk_columns: experience_pk_columns_input!
  ) {
    updateExperienceOne(_set: $experience, pk_columns: $pk_columns) {
      ...updateExperienceOne_experienceOne
    }
  }
  ${UpdateExperienceOne_ExperienceOneFragmentDoc}
`

export function useUpdateExperienceOneMutation() {
  return Urql.useMutation<
    UpdateExperienceOneMutation,
    UpdateExperienceOneMutationVariables
  >(UpdateExperienceOneDocument)
}
export const DummyDocument = gql`
  query dummy {
    __typename
  }
`

export function useDummyQuery(
  options?: Omit<Urql.UseQueryArgs<DummyQueryVariables>, 'query'>
) {
  return Urql.useQuery<DummyQuery>({ query: DummyDocument, ...options })
}
export const ProjectsDocument = gql`
  query projects(
    $distinct_on: [project_select_column!]
    $limit: Int
    $offset: Int
    $order_by: [project_order_by!]
    $where: project_bool_exp
  ) {
    projects(
      distinct_on: $distinct_on
      limit: $limit
      offset: $offset
      order_by: $order_by
      where: $where
    ) {
      ...projects_projects
    }
  }
  ${Projects_ProjectsFragmentDoc}
`

export function useProjectsQuery(
  options?: Omit<Urql.UseQueryArgs<ProjectsQueryVariables>, 'query'>
) {
  return Urql.useQuery<ProjectsQuery>({ query: ProjectsDocument, ...options })
}
export const ProjectDocument = gql`
  query project($id: uuid!) {
    project(id: $id) {
      ...project_project
    }
  }
  ${Project_ProjectFragmentDoc}
`

export function useProjectQuery(
  options: Omit<Urql.UseQueryArgs<ProjectQueryVariables>, 'query'>
) {
  return Urql.useQuery<ProjectQuery>({ query: ProjectDocument, ...options })
}
export const InsertProjectDocument = gql`
  mutation insertProject(
    $project: project_insert_input!
    $on_conflict: project_on_conflict
  ) {
    insertProject(object: $project, on_conflict: $on_conflict) {
      ...insertProject_project
    }
  }
  ${InsertProject_ProjectFragmentDoc}
`

export function useInsertProjectMutation() {
  return Urql.useMutation<
    InsertProjectMutation,
    InsertProjectMutationVariables
  >(InsertProjectDocument)
}
export const UpdateProjectDocument = gql`
  mutation updateProject(
    $project: project_set_input
    $pk_columns: project_pk_columns_input!
  ) {
    updateProject(_set: $project, pk_columns: $pk_columns) {
      ...updateProject_project
    }
  }
  ${UpdateProject_ProjectFragmentDoc}
`

export function useUpdateProjectMutation() {
  return Urql.useMutation<
    UpdateProjectMutation,
    UpdateProjectMutationVariables
  >(UpdateProjectDocument)
}
export const SkillGroupsDocument = gql`
  query skillGroups(
    $distinct_on: [skillGroup_select_column!]
    $limit: Int
    $offset: Int
    $order_by: [skillGroup_order_by!]
    $where: skillGroup_bool_exp
  ) {
    skillGroups(
      distinct_on: $distinct_on
      limit: $limit
      offset: $offset
      order_by: $order_by
      where: $where
    ) {
      ...skill_groups
      skills {
        ...skillsGroup_skills
      }
    }
  }
  ${Skill_GroupsFragmentDoc}
  ${SkillsGroup_SkillsFragmentDoc}
`

export function useSkillGroupsQuery(
  options?: Omit<Urql.UseQueryArgs<SkillGroupsQueryVariables>, 'query'>
) {
  return Urql.useQuery<SkillGroupsQuery>({
    query: SkillGroupsDocument,
    ...options
  })
}
export const SkillsDocument = gql`
  query skills(
    $distinct_on: [skill_select_column!]
    $limit: Int
    $offset: Int
    $order_by: [skill_order_by!]
    $where: skill_bool_exp
  ) {
    skills(
      distinct_on: $distinct_on
      limit: $limit
      offset: $offset
      order_by: $order_by
      where: $where
    ) {
      ...skills_skills
    }
  }
  ${Skills_SkillsFragmentDoc}
`

export function useSkillsQuery(
  options?: Omit<Urql.UseQueryArgs<SkillsQueryVariables>, 'query'>
) {
  return Urql.useQuery<SkillsQuery>({ query: SkillsDocument, ...options })
}
export const SkillDocument = gql`
  query skill($id: uuid!) {
    skill(id: $id) {
      ...skill_skills
    }
  }
  ${Skill_SkillsFragmentDoc}
`

export function useSkillQuery(
  options: Omit<Urql.UseQueryArgs<SkillQueryVariables>, 'query'>
) {
  return Urql.useQuery<SkillQuery>({ query: SkillDocument, ...options })
}
export const InsertSkillDocument = gql`
  mutation insertSkill(
    $skill: skill_insert_input!
    $on_conflict: skill_on_conflict
  ) {
    insertSkill(object: $skill, on_conflict: $on_conflict) {
      ...insertSkill_skill
    }
  }
  ${InsertSkill_SkillFragmentDoc}
`

export function useInsertSkillMutation() {
  return Urql.useMutation<InsertSkillMutation, InsertSkillMutationVariables>(
    InsertSkillDocument
  )
}
export const UpdateSkillDocument = gql`
  mutation updateSkill(
    $skill: skill_set_input
    $pk_columns: skill_pk_columns_input!
  ) {
    updateSkill(_set: $skill, pk_columns: $pk_columns) {
      ...updateSkill_skill
    }
  }
  ${UpdateSkill_SkillFragmentDoc}
`

export function useUpdateSkillMutation() {
  return Urql.useMutation<UpdateSkillMutation, UpdateSkillMutationVariables>(
    UpdateSkillDocument
  )
}
