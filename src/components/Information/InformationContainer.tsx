import { Container, Grid } from '@mui/material'
import React from 'react'
import information from './information'
import { InformationText } from './InformationText'
import Img from '../../assets/ivan.jpg'

export const InformationContainer: React.FC = () => {
  const printInformation = () => {
    return information.map((info, index) => (
      <Grid item key={index}>
        <InformationText
          title={info.title}
          text={info.text}
          style={{ overflowWrap: 'anywhere' }}
        />
      </Grid>
    ))
  }

  return (
    <Container
      style={{
        backgroundColor: '#DEDEDE',
        position: 'absolute',
        maxWidth: '25%',
        // height: '85%',
        textAlign: 'center'
      }}
    >
      <img
        src={Img}
        alt="Avatar"
        style={{
          width: '55%',
          height: 'auto',
          marginTop: '25px',
          marginBottom: '25px'
        }}
      />
      <Grid
        container
        direction="column"
        style={{
          textAlign: 'left',
          marginBottom: '50px'
        }}
        rowSpacing={3}
      >
        {printInformation()}
      </Grid>
    </Container>
  )
}
