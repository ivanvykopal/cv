import { Box, Typography } from '@mui/material'
import React from 'react'

interface Props {
  title: string
  text: string
  style?: any
}

export const InformationText: React.FC<Props> = ({ title, text, style }) => {
  return (
    <Box style={style}>
      <Typography
        variant="h6"
        style={{
          fontWeight: 600
        }}
      >
        {`${title}: `}
      </Typography>
      <Typography variant="h6">{text}</Typography>
    </Box>
  )
}
