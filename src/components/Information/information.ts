const information = [
  {
    title: 'Name',
    text: 'Bc. Ivan Vykopal'
  },
  {
    title: 'Address',
    text: 'Trnava 91701, Slovak Republic'
  },
  {
    title: 'Email',
    text: 'ivan.vykopal@gmail.com'
  },
  {
    title: 'Date of birth',
    text: '24. 06. 1999'
  }
]

export default information
