import {
  Experience_Constraint,
  Experience_Update_Column,
  Order_By,
  useExperienceListQuery,
  useExperienceOneQuery
} from '../../generated/graphql'
import { Experience } from '../interfaces'

export const useExperienceList = () => {
  return useExperienceListQuery({
    variables: {
      order_by: {
        from: Order_By.Desc
      },
      where: {
        isDeleted: {
          _eq: false
        }
      }
    }
  })
}

export const useExperienceOne = (id: any) => {
  return useExperienceOneQuery({
    variables: {
      id: id
    }
  })
}

export const useInsertExperienceOne = (
  values: Experience,
  insertMutation: any
) => {
  return insertMutation({
    experience: {
      name: values.name,
      firmName: values.firmName,
      type: values.type,
      position: values.position,
      from: values.from,
      to: values.to === '' ? undefined : values.to,
      note: values.note === '' ? undefined : values.note
    },
    on_conflict: {
      constraint: Experience_Constraint.ExperiencePkey,
      update_columns: [
        Experience_Update_Column.CreatedAt,
        Experience_Update_Column.FirmName,
        Experience_Update_Column.From,
        Experience_Update_Column.Id,
        Experience_Update_Column.IsDeleted,
        Experience_Update_Column.Name,
        Experience_Update_Column.Note,
        Experience_Update_Column.Position,
        Experience_Update_Column.To,
        Experience_Update_Column.Type,
        Experience_Update_Column.UpdatedAt
      ]
    }
  })
}

export const useUpdateExperienceOne = (
  values: Experience,
  id: any,
  updateMutation: any
) => {
  return updateMutation({
    experience: {
      name: values.name,
      firmName: values.firmName,
      type: values.type,
      position: values.position,
      from: values.from,
      to: values.to === '' ? undefined : values.to,
      note: values.note === '' ? undefined : values.note
    },
    pk_columns: { id: id }
  })
}

export const useDeleteExperience = (id: any, updateMutation: any) => {
  return updateMutation({
    experience: { isDeleted: true },
    pk_columns: { id: id }
  })
}
