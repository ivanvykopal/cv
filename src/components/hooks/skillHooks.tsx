import {
  Skill_Constraint,
  Skill_Update_Column,
  useSkillGroupsQuery,
  useSkillQuery
} from '../../generated/graphql'
import { Skill } from '../interfaces'

export const useSkillGroupsList = () => {
  return useSkillGroupsQuery({
    variables: {
      where: {
        isDeleted: {
          _eq: false
        }
      }
    }
  })
}

export const useSkillOne = (id: any) => {
  return useSkillQuery({
    variables: {
      id: id
    }
  })
}

export const useInsertSkillOne = (values: Skill, insertMutation: any) => {
  return insertMutation({
    skill: {
      skillGroupsId: values.skillGroupsId,
      level: values.level === '' ? undefined : values.level,
      name: values.name
    },
    on_conflict: {
      constraint: Skill_Constraint.SkillsPkey,
      update_columns: [
        Skill_Update_Column.CreatedAt,
        Skill_Update_Column.Group,
        Skill_Update_Column.Id,
        Skill_Update_Column.IsDeleted,
        Skill_Update_Column.Level,
        Skill_Update_Column.Name,
        Skill_Update_Column.UpdatedAt,
        Skill_Update_Column.SkillGroupsId
      ]
    }
  })
}

export const useDeleteSkill = (id: any, updateMutation: any) => {
  return updateMutation({
    skill: { isDeleted: true },
    pk_columns: { id: id }
  })
}

export const useUpdateSkillOne = (
  values: Skill,
  id: any,
  updateMutation: any
) => {
  return updateMutation({
    skill: {
      name: values.name,
      level: values.name === '' ? undefined : values.level,
      skillGroupsId: values.skillGroupsId
    },
    pk_columns: { id: id }
  })
}
