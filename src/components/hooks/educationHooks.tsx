import {
  Education_Constraint,
  Education_Update_Column,
  Order_By,
  useEducationListQuery,
  useEducationOneQuery
} from '../../generated/graphql'
import { Education } from '../interfaces'

export const useEducationList = () => {
  return useEducationListQuery({
    variables: {
      order_by: {
        from: Order_By.Desc
      },
      where: {
        isDeleted: {
          _eq: false
        }
      }
    }
  })
}

export const useEducationOne = (id: any) => {
  return useEducationOneQuery({
    variables: {
      id: id
    }
  })
}

export const useInsertEduationOne = (
  values: Education,
  insertMutation: any
) => {
  return insertMutation({
    education: {
      school: values.school,
      degree: values.degree,
      fieldOfStudy: values.fieldOfStudy,
      from: values.from,
      to: values.to === '' ? undefined : values.to,
      description: values.description === '' ? undefined : values.description
    },
    on_conflict: {
      constraint: Education_Constraint.EducationPkey,
      update_columns: [
        Education_Update_Column.CreatedAt,
        Education_Update_Column.From,
        Education_Update_Column.Id,
        Education_Update_Column.IsDeleted,
        Education_Update_Column.School,
        Education_Update_Column.Description,
        Education_Update_Column.Degree,
        Education_Update_Column.To,
        Education_Update_Column.UpdatedAt,
        Education_Update_Column.FieldOfStudy
      ]
    }
  })
}

export const useUpdateEducationOne = (
  values: Education,
  id: any,
  updateMutation: any
) => {
  return updateMutation({
    education: {
      school: values.school,
      degree: values.degree,
      fieldOfStudy: values.fieldOfStudy,
      from: values.from,
      to: values.to === '' ? undefined : values.to,
      description: values.description === '' ? undefined : values.description
    },
    pk_columns: { id: id }
  })
}

export const useDeleteEducation = (id: any, updateMutation: any) => {
  return updateMutation({
    education: { isDeleted: true },
    pk_columns: { id: id }
  })
}
