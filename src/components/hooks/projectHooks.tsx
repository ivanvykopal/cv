import {
  Project_Constraint,
  Project_Update_Column,
  useProjectQuery,
  useProjectsQuery
} from '../../generated/graphql'
import { Project } from '../interfaces'

export const useProjectsList = () => {
  return useProjectsQuery({
    variables: {
      where: {
        isDeleted: {
          _eq: false
        }
      }
    }
  })
}

export const useProjectOne = (id: any) => {
  return useProjectQuery({
    variables: {
      id: id
    }
  })
}

export const insertProjectOne = (values: Project, insertMutation: any) => {
  return insertMutation({
    project: {
      name: values.name,
      imageUrl: values.imageUrl === '' ? undefined : values.imageUrl,
      description: values.description === '' ? undefined : values.description,
      githubUrl: values.githubUrl
    },
    on_conflict: {
      constraint: Project_Constraint.ProjectsPkey,
      update_columns: [
        Project_Update_Column.CreatedAt,
        Project_Update_Column.Description,
        Project_Update_Column.GithubUrl,
        Project_Update_Column.Id,
        Project_Update_Column.ImageUrl,
        Project_Update_Column.IsDeleted,
        Project_Update_Column.Name,
        Project_Update_Column.UpdatedAt
      ]
    }
  })
}

export const useUpdateProjectOne = (
  values: Project,
  id: any,
  updateMutation: any
) => {
  return updateMutation({
    project: {
      name: values.name,
      imageUrl: values.imageUrl === '' ? undefined : values.imageUrl,
      githubUrl: values.githubUrl,
      description: values.description === '' ? undefined : values.description
    },
    pk_columns: { id: id }
  })
}

export const useDeleteProject = (id: any, updateMutation: any) => {
  return updateMutation({
    project: { isDeleted: true },
    pk_columns: { id: id }
  })
}
