import { Add } from '@mui/icons-material'
import { Box } from '@mui/material'
import React from 'react'

interface Props {
  onClick: (event: any) => void
}

export const AddButton: React.FC<Props> = ({ onClick }) => {
  return (
    <Box
      onClick={onClick}
      style={{
        textAlign: 'center',
        backgroundColor: '#F2F2F2',
        padding: '5px',
        borderRadius: '25px',
        border: '2px dashed black',
        cursor: 'pointer'
      }}
    >
      <Add fontSize="large" />
    </Box>
  )
}
