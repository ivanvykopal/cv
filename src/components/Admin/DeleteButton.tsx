import { Delete } from '@mui/icons-material'
import { Button } from '@mui/material'
import React from 'react'

interface Props {
  onClick: (event: any) => void
}

export const DeleteButton: React.FC<Props> = ({ onClick }) => {
  return (
    <Button
      onClick={onClick}
      style={{
        color: 'black'
      }}
    >
      <Delete />
    </Button>
  )
}
