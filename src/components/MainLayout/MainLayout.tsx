import { Box } from '@mui/material'
import React from 'react'
import { Outlet } from 'react-router-dom'
import { TopBar } from '../TopBar/TopBar'

const MainLayout: React.FC = () => {
  return (
    <Box>
      <TopBar />
      <Outlet />
    </Box>
  )
}

export default MainLayout
