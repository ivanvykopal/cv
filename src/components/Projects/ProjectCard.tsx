import { Box, CircularProgress, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { EditButton, DeleteButton } from '../Admin'

interface Props {
  data: any
  dialog: boolean
  admin: boolean
  onDelete?: (event: any, id: any) => void
  setEditing?: (value: boolean) => void
}

export const ProjectCard: React.FC<Props> = ({
  data,
  dialog,
  admin,
  onDelete,
  setEditing
}) => {
  const [project, setProject] = useState(data)
  const [hover, setHover] = useState(false)

  useEffect(() => {
    setProject(data)
  }, [data])

  const handleMouseIn = () => {
    setHover(true)
  }

  const handleMouseOut = () => {
    setHover(false)
  }

  const handleEdit = (event: any) => {
    event.stopPropagation()
    setEditing?.(true)
  }

  const handleAdmin = () => {
    if (admin && hover) {
      return (
        <Box
          style={{
            padding: '5px 5px'
          }}
        >
          <EditButton onClick={handleEdit} />
          <DeleteButton
            onClick={(event: any) => onDelete?.(event, project.id)}
          />
        </Box>
      )
    }
  }

  const showNoteIfExist = () => {
    if (project.description) {
      return (
        <Box
          style={{
            paddingTop: '15px'
          }}
        >
          <Typography
            style={{
              fontWeight: 600
            }}
          >
            Description:{' '}
          </Typography>
          <Typography>{project.description}</Typography>
        </Box>
      )
    }
  }

  if (dialog && !project?.name) {
    return <CircularProgress />
  }

  return (
    <Box
      onMouseOver={handleMouseIn}
      onMouseLeave={handleMouseOut}
      style={{
        display: 'flex',
        justifyContent: 'flex-end'
      }}
    >
      <Box style={{ marginRight: 'auto', padding: '15px 25px' }}>
        <Typography
          style={{
            fontSize: 18,
            fontWeight: 600
          }}
        >
          {project.name}
        </Typography>
        <Typography
          style={{
            display: 'inline',
            fontWeight: 600
          }}
        >
          {`URL: `}
        </Typography>
        <Typography
          style={{
            display: 'inline'
          }}
        >
          <a href={project.githubUrl}>{project.githubUrl}</a>
        </Typography>
        {showNoteIfExist()}
      </Box>
      {handleAdmin()}
    </Box>
  )
}
