import { Box } from '@mui/material'
import React from 'react'
import { UseQueryState } from 'urql'
import { ProjectsQuery } from '../../generated/graphql'
import { ProjectsList } from './ProjectsList'

interface Props {
  data: UseQueryState<ProjectsQuery, object>
  admin: boolean
}

export const ProjectsContainer: React.FC<Props> = ({ data, admin }) => {
  return (
    <Box
      id="projects"
      style={{
        marginTop: '25px'
      }}
    >
      <ProjectsList data={data} admin={admin} />
    </Box>
  )
}
