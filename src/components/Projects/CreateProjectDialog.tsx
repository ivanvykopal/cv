import { Box, Button, Dialog, DialogTitle, TextField } from '@mui/material'
import { useFormik } from 'formik'
import React from 'react'
import { toast } from 'react-toastify'
import { useInsertProjectMutation } from '../../generated/graphql'
import { insertProjectOne } from '../hooks'

interface Props {
  open: boolean
  onClose: () => void
}

export const CreateProjectDialog: React.FC<Props> = ({ open, onClose }) => {
  const [insertedData, insertMutation] = useInsertProjectMutation()

  const formik = useFormik({
    initialValues: {
      name: '',
      imageUrl: '',
      description: '',
      githubUrl: ''
    },
    onSubmit: (values) => {
      toast.promise(insertProjectOne(values, insertMutation), {
        pending: 'Creating new experience',
        success: 'New experience created',
        error: 'Error while creating new experience'
      })

      onClose()
      formik.resetForm()
    }
  })

  const handleClose = () => {
    onClose()
    formik.resetForm()
  }

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>Experience</DialogTitle>
      <Box
        style={{
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingBottom: '50px'
        }}
      >
        <form onSubmit={formik.handleSubmit}>
          <Box
            sx={{
              width: '100%',
              height: '100%'
            }}
          >
            <TextField
              type="text"
              name="name"
              id="name"
              title="Name"
              fullWidth
              required
              margin="normal"
              label="Name"
              value={formik.values.name}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.name ? true : false
              }}
            />
            <TextField
              type="text"
              name="imageUrl"
              id="imageUrl"
              title="Image URL"
              fullWidth
              margin="normal"
              label="Image URL"
              value={formik.values.imageUrl}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.imageUrl ? true : false
              }}
            />
            <TextField
              type="text"
              name="githubUrl"
              id="githubUrl"
              title="GitHub/GitLab URL"
              fullWidth
              required
              margin="normal"
              label="GitHub/GitLab URL"
              value={formik.values.githubUrl}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.githubUrl ? true : false
              }}
            />
            <TextField
              id="description"
              label="Description"
              multiline
              fullWidth
              minRows={3}
              maxRows={6}
              value={formik.values.description}
              onChange={formik.handleChange}
              style={{
                paddingBottom: '30px'
              }}
            />
            <Box>
              <Button type="submit" variant="contained" fullWidth>
                Create Project
              </Button>
            </Box>
          </Box>
        </form>
      </Box>
    </Dialog>
  )
}
