import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogTitle,
  TextField
} from '@mui/material'
import { useFormik } from 'formik'
import React, { useEffect } from 'react'
import { toast } from 'react-toastify'
import { useUpdateProjectMutation } from '../../generated/graphql'
import { useProjectOne, useUpdateProjectOne } from '../hooks'
import { ProjectCard } from './ProjectCard'

interface Props {
  id: any
  open: boolean
  admin: boolean
  onClose: () => void
  editing: boolean
}

export const ProjectDialog: React.FC<Props> = ({
  id,
  open,
  onClose,
  admin,
  editing
}) => {
  const [updatedData, updateMutation] = useUpdateProjectMutation()
  const [data] = useProjectOne(id)

  useEffect(() => {
    formik.setFieldValue('name', data?.data?.project?.name ?? '')
    formik.setFieldValue('imageUrl', data?.data?.project?.imageUrl ?? '')
    formik.setFieldValue('description', data?.data?.project?.description ?? '')
    formik.setFieldValue('githubUrl', data?.data?.project?.githubUrl ?? '')
  }, [data?.data])

  const formik = useFormik({
    initialValues: {
      name: '',
      imageUrl: '',
      description: '',
      githubUrl: ''
    },
    onSubmit: (values) => {
      toast.promise(useUpdateProjectOne(values, id, updateMutation), {
        pending: 'Updating project',
        success: 'Project updated',
        error: 'Error while updating project'
      })

      handleClose()
    }
  })

  const handleClose = () => {
    onClose()
    formik.resetForm()
  }

  const handleAdmin = () => {
    if (admin && editing) {
      return (
        <Box
          style={{
            paddingLeft: '50px',
            paddingRight: '50px',
            paddingBottom: '50px'
          }}
        >
          <form onSubmit={formik.handleSubmit}>
            <Box
              sx={{
                width: '100%',
                height: '100%'
              }}
            >
              <TextField
                type="text"
                name="name"
                id="name"
                title="Name"
                fullWidth
                required
                margin="normal"
                label="Name"
                value={formik.values.name}
                onChange={formik.handleChange}
                InputLabelProps={{
                  shrink: formik.values.name ? true : false
                }}
              />
              <TextField
                type="text"
                name="imageUrl"
                id="imageUrl"
                title="Image URL"
                fullWidth
                margin="normal"
                label="Image URL"
                value={formik.values.imageUrl}
                onChange={formik.handleChange}
                InputLabelProps={{
                  shrink: formik.values.imageUrl ? true : false
                }}
              />
              <TextField
                type="text"
                name="githubUrl"
                id="githubUrl"
                title="GitHub/GitLab URL"
                fullWidth
                required
                margin="normal"
                label="GitHub/GitLab URL"
                value={formik.values.githubUrl}
                onChange={formik.handleChange}
                InputLabelProps={{
                  shrink: formik.values.githubUrl ? true : false
                }}
              />
              <TextField
                id="description"
                label="Description"
                multiline
                fullWidth
                minRows={3}
                maxRows={6}
                value={formik.values.description}
                onChange={formik.handleChange}
                style={{
                  paddingBottom: '30px'
                }}
              />
              <Box>
                <Button type="submit" variant="contained" fullWidth>
                  Update Project
                </Button>
              </Box>
            </Box>
          </form>
        </Box>
      )
    }

    return (
      <Box
        style={{
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingBottom: '50px'
        }}
      >
        <ProjectCard data={data?.data?.project} dialog={true} admin={false} />
      </Box>
    )
  }

  if (data?.fetching)
    return (
      <Dialog onClose={handleClose} open={open}>
        <DialogTitle>Project</DialogTitle>
        <Box
          style={{
            padding: '50px'
          }}
        >
          <CircularProgress />
        </Box>
      </Dialog>
    )

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>
        Experience{' '}
        {data?.data?.project?.name ? ` - ${data?.data?.project?.name}` : ''}
      </DialogTitle>
      {handleAdmin()}
    </Dialog>
  )
}
