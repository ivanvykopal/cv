import { Box, CircularProgress, Typography } from '@mui/material'
import React, { useState } from 'react'
import { toast } from 'react-toastify'
import { UseQueryState } from 'urql'
import {
  ProjectsQuery,
  useUpdateProjectMutation
} from '../../generated/graphql'
import { AddButton } from '../Admin'
import { ProjectCard } from './ProjectCard'
import './index.css'
import { ProjectDialog } from './ProjectsDialog'
import { CreateProjectDialog } from './CreateProjectDialog'
import { useDeleteProject } from '../hooks'

interface Props {
  data: UseQueryState<ProjectsQuery, object>
  admin: boolean
}

export const ProjectsList: React.FC<Props> = ({ data, admin }) => {
  const [id, setId] = useState(undefined)
  const [showDialog, setShowDialog] = useState<boolean>(false)
  const [showCreatingDialog, setShowCreatingDialog] = useState<boolean>(false)
  const [editing, setEditing] = useState(false)

  const [updateData, updateMutation] = useUpdateProjectMutation()

  const closeDialog = () => {
    setShowDialog(false)
    setShowCreatingDialog(false)
    setId(undefined)
    setEditing(false)
  }

  const onDelete = (e: any, id: any) => {
    e.stopPropagation()
    toast.promise(useDeleteProject(id, updateMutation), {
      pending: 'Deleting project',
      success: 'Project deleted',
      error: 'Error while deleting project'
    })
  }

  const createProjectItems = () => {
    return data.data?.projects.map((project) => {
      return (
        <Box
          className="project-item"
          key={project.id}
          onClick={() => {
            setId(project.id)
            setShowDialog(true)
          }}
        >
          <ProjectCard
            data={project}
            dialog={false}
            admin={admin}
            onDelete={onDelete}
            setEditing={(value) => {
              setEditing(value)
              setId(project.id)
              if (value) setShowDialog(true)
            }}
          />
        </Box>
      )
    })
  }

  const handleAdminAdd = () => {
    if (admin)
      return (
        <Box
          style={{
            padding: '10px'
          }}
        >
          <AddButton onClick={() => setShowCreatingDialog(true)} />
        </Box>
      )
  }

  if (data.fetching) {
    return <CircularProgress />
  }

  return (
    <Box>
      <Box
        style={{
          alignItems: 'center'
        }}
      >
        <Typography
          variant="h5"
          style={{
            fontWeight: 600
          }}
        >
          Projects
        </Typography>
      </Box>
      {createProjectItems()}
      <ProjectDialog
        id={id}
        open={showDialog}
        onClose={closeDialog}
        admin={admin}
        editing={editing}
      />
      <CreateProjectDialog
        open={admin && showCreatingDialog}
        onClose={closeDialog}
      />
      {handleAdminAdd()}
    </Box>
  )
}
