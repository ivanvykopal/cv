import { Box, Typography } from '@mui/material'
import React from 'react'

export const PageNotFound: React.FC = () => {
  return (
    <Box>
      <Typography align="center" color="textPrimary" variant="h1">
        404: The page you are looking for isn’t here
      </Typography>
    </Box>
  )
}
