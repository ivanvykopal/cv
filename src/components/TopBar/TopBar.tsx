import React, { useState, useEffect } from 'react'
import { Link as RouterLink } from 'react-router-dom'
import {
  AppBar,
  Box,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  SwipeableDrawer,
  Toolbar,
  Typography
} from '@mui/material'
import MenuIcon from '@mui/icons-material/Menu'
import { MEDIUM } from './screen_sizes'

import Link from '@material-ui/core/Link'
import WorkOutlineIcon from '@mui/icons-material/WorkOutline'
import SchoolOutlinedIcon from '@mui/icons-material/SchoolOutlined'
import StarBorderIcon from '@mui/icons-material/StarBorder'
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings'
import SourceOutlinedIcon from '@mui/icons-material/SourceOutlined'

export const TopBar: React.FC = () => {
  const [drawerOpen, setDrawerOpen] = useState(false)
  const [screenWidth, setScreenWidth] = useState(window.innerWidth)

  const handleDrawerOpen = () => {
    setDrawerOpen(true)
  }

  const handleDrawerClose = () => {
    setDrawerOpen(false)
  }

  useEffect(() => {
    updateWidth()
    window.addEventListener('resize', updateWidth)
    return () => window.removeEventListener('resize', updateWidth)
  }, [])

  const updateWidth = () => {
    const newWidth = window.innerWidth
    setScreenWidth(newWidth)
  }

  const linkStyle = {
    color: '#1976d2',
    textDecoration: 'none'
  }

  return (
    <Box>
      <SwipeableDrawer
        anchor="left"
        open={drawerOpen}
        onOpen={handleDrawerOpen}
        onClose={handleDrawerClose}
        style={{
          width: '200px'
        }}
      >
        <List>
          <Link href="#experience" style={linkStyle}>
            <ListItem button onClick={handleDrawerClose}>
              <ListItemIcon>
                <WorkOutlineIcon />
              </ListItemIcon>
              <ListItemText>EXPERIENCE</ListItemText>
            </ListItem>
          </Link>
          <Link href="#education" style={linkStyle}>
            <ListItem button onClick={handleDrawerClose}>
              <ListItemIcon>
                <SchoolOutlinedIcon />
              </ListItemIcon>
              <ListItemText>EDUCATION</ListItemText>
            </ListItem>
          </Link>
          <Link href="#skills" style={linkStyle}>
            <ListItem button onClick={handleDrawerClose}>
              <ListItemIcon>
                <StarBorderIcon />
              </ListItemIcon>
              <ListItemText>SKILLS</ListItemText>
            </ListItem>
          </Link>
          <Link href="#projects" style={linkStyle}>
            <ListItem button onClick={handleDrawerClose}>
              <ListItemIcon>
                <SourceOutlinedIcon />
              </ListItemIcon>
              <ListItemText>PROJECTS</ListItemText>
            </ListItem>
          </Link>
        </List>
      </SwipeableDrawer>
      <AppBar style={{ position: 'fixed' }}>
        <Toolbar>
          <Grid
            container
            direction="row"
            justifyContent="flex-start"
            alignItems="stretch"
          >
            <Link
              href="/"
              style={{
                color: '#fff',
                textDecoration: 'none'
              }}
            >
              <Box
                style={{
                  display: 'flex',
                  textAlign: 'center'
                }}
              >
                <Typography
                  variant="h6"
                  style={{
                    margin: '10px'
                  }}
                >
                  Ivan Vykopal
                </Typography>
              </Box>
            </Link>
          </Grid>
          {screenWidth > MEDIUM && (
            <>
              <List style={{ display: 'flex', flexDirection: 'row' }}>
                <Link href="#experience" style={{ textDecoration: 'none' }}>
                  <ListItem button>
                    <ListItemText style={{ color: '#fff' }}>
                      EXPERIENCE
                    </ListItemText>
                  </ListItem>
                </Link>
                <Link href="#education" style={{ textDecoration: 'none' }}>
                  <ListItem button>
                    <ListItemText style={{ color: '#fff' }}>
                      EDUCATION
                    </ListItemText>
                  </ListItem>
                </Link>
                <Link href="#skills" style={{ textDecoration: 'none' }}>
                  <ListItem button>
                    <ListItemText style={{ color: '#fff' }}>
                      SKILLS
                    </ListItemText>
                  </ListItem>
                </Link>
                <Link href="#projects" style={{ textDecoration: 'none' }}>
                  <ListItem button>
                    <ListItemText style={{ color: '#fff' }}>
                      PROJECTS
                    </ListItemText>
                  </ListItem>
                </Link>
              </List>
            </>
          )}
          {screenWidth <= MEDIUM && (
            <Grid
              container
              direction="row"
              justifyContent="flex-end"
              alignItems="center"
              spacing={6}
            >
              <Grid item>
                <IconButton
                  style={{ color: '#ffffff', fontSize: '20px' }}
                  onClick={handleDrawerOpen}
                >
                  <MenuIcon fontSize="large" />
                </IconButton>
              </Grid>
            </Grid>
          )}
          <RouterLink to="/admin">
            <ListItem button>
              <ListItemIcon style={{ justifyContent: 'center' }}>
                <AdminPanelSettingsIcon style={{ color: '#ffffff' }} />
              </ListItemIcon>
            </ListItem>
          </RouterLink>
        </Toolbar>
      </AppBar>
    </Box>
  )
}
