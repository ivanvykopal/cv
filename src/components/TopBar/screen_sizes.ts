export const EXTRA_SMALL = 600
export const MEDIUM = 768
export const LARGE = 992
export const EXTRA_LARGE = 1200
