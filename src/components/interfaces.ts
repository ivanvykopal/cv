export interface Education {
  school: string
  degree: string
  fieldOfStudy: string
  from: Date | string
  to: string
  description: string
}

export interface Experience {
  name: string
  firmName: string
  type: string
  position: string
  from: Date | string
  to: string
  note: string
}

export interface Project {
  name: string
  imageUrl: string
  description: string
  githubUrl: string
}

export interface Skill {
  skillGroupsId: string
  level: string
  name: string
}
