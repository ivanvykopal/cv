import { Box } from '@mui/material'
import React from 'react'
import { UseQueryState } from 'urql'
import { EducationListQuery } from '../../generated/graphql'
import { EducationList } from './EducationList'

interface Props {
  data: UseQueryState<EducationListQuery, object>
  admin: boolean
}

export const EducationContainer: React.FC<Props> = ({ data, admin }) => {
  return (
    <Box
      id="education"
      style={{
        marginTop: '25px'
      }}
    >
      <EducationList data={data} admin={admin} />
    </Box>
  )
}
