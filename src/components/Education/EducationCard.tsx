import { Box, CircularProgress, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { DeleteButton, EditButton } from '../Admin'
import { formatDate } from '../utils'

interface Props {
  data: any
  dialog: boolean
  admin: boolean
  onDelete?: (event: any, id: any) => void
  setEditing?: (value: boolean) => void
}

export const EducationCard: React.FC<Props> = ({
  data,
  dialog,
  admin,
  onDelete,
  setEditing
}) => {
  const [education, setEducation] = useState(data)
  const [hover, setHover] = useState(false)

  useEffect(() => {
    setEducation(data)
  }, [data])

  const handleMouseIn = () => {
    setHover(true)
  }

  const handleMouseOut = () => {
    setHover(false)
  }

  const handleEdit = (event: any) => {
    event.stopPropagation()
    setEditing?.(true)
  }

  const handleAdmin = () => {
    if (admin && hover) {
      return (
        <Box
          style={{
            padding: '5px 5px'
          }}
        >
          <EditButton onClick={handleEdit} />
          <DeleteButton
            onClick={(event: any) => onDelete?.(event, education.id)}
          />
        </Box>
      )
    }
  }

  const showNoteIfExist = () => {
    if (dialog && education?.description) {
      return (
        <Box
          style={{
            paddingTop: '25px'
          }}
        >
          <Typography
            style={{
              fontSize: 18,
              fontWeight: 600
            }}
          >
            Note:{' '}
          </Typography>
          <Typography>{education.description}</Typography>
        </Box>
      )
    }
  }

  if (dialog && !education?.degree) {
    return <CircularProgress />
  }

  return (
    <Box
      onMouseOver={handleMouseIn}
      onMouseLeave={handleMouseOut}
      style={{
        display: 'flex',
        justifyContent: 'flex-end'
      }}
    >
      <Box style={{ marginRight: 'auto', padding: '10px 25px' }}>
        <Typography
          style={{
            fontWeight: 800
          }}
        >
          {education.school}
        </Typography>
        <Typography>
          {`${education.degree}, ${education.fieldOfStudy ?? ''}`}
        </Typography>
        <Typography>
          {`${formatDate(education.from)} - ${formatDate(education.to)}`}
        </Typography>
        {showNoteIfExist()}
      </Box>
      {handleAdmin()}
    </Box>
  )
}
