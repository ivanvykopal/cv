import React, { useState } from 'react'
import {
  EducationListQuery,
  useUpdateEducationOneMutation
} from '../../generated/graphql'
import { UseQueryState } from 'urql'
import { Box, CircularProgress, Typography } from '@mui/material'
import './index.css'
import { EducationCard } from './EducationCard'
import { EducationDialog } from './EducationDialog'
import { AddButton } from '../Admin'
import { toast } from 'react-toastify'
import { CreateEducationDialog } from './CreateEducationDialog'
import { useDeleteEducation } from '../hooks'

interface Props {
  data: UseQueryState<EducationListQuery, object>
  admin: boolean
}

export const EducationList: React.FC<Props> = ({ data, admin }) => {
  const [id, setId] = useState(undefined)
  const [showDialog, setShowDialog] = useState<boolean>(false)
  const [showCreatingDialog, setShowCreatingDialog] = useState<boolean>(false)
  const [editing, setEditing] = useState(false)

  const [updatedData, updateMutation] = useUpdateEducationOneMutation()

  const closeDialog = () => {
    setShowDialog(false)
    setShowCreatingDialog(false)
    setId(undefined)
    setEditing(false)
  }

  const onDelete = (e: any, id: any) => {
    e.stopPropagation()
    toast.promise(useDeleteEducation(id, updateMutation), {
      pending: 'Deleting education',
      success: 'Education deleted',
      error: 'Error while deleting education'
    })
  }

  const createEducationItems = () => {
    return data.data?.educationList.map((education) => {
      return (
        <Box
          className="education-item"
          key={education.id}
          onClick={() => {
            setId(education.id)
            setShowDialog(true)
          }}
        >
          <EducationCard
            data={education}
            dialog={false}
            admin={admin}
            onDelete={onDelete}
            setEditing={(value) => {
              setEditing(value)
              setId(education.id)
              if (value) setShowDialog(true)
            }}
          />
        </Box>
      )
    })
  }

  const handleAdminAdd = () => {
    if (admin)
      return (
        <Box
          style={{
            padding: '10px'
          }}
        >
          <AddButton onClick={() => setShowCreatingDialog(true)} />
        </Box>
      )
  }

  if (data.fetching) {
    return <CircularProgress />
  }

  return (
    <Box>
      <Box
        style={{
          alignItems: 'center'
        }}
      >
        <Typography
          variant="h5"
          style={{
            fontWeight: 600
          }}
        >
          Education
        </Typography>
      </Box>
      {createEducationItems()}
      <EducationDialog
        id={id}
        open={showDialog}
        onClose={closeDialog}
        admin={admin}
        editing={editing}
      />
      <CreateEducationDialog
        open={admin && showCreatingDialog}
        onClose={closeDialog}
      />
      {handleAdminAdd()}
    </Box>
  )
}
