import { Box, Button, Dialog, DialogTitle, TextField } from '@mui/material'
import { useFormik } from 'formik'
import React from 'react'
import { toast } from 'react-toastify'
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker'
import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { useInsertEduationOne } from '../hooks'
import { useInsertEducationOneMutation } from '../../generated/graphql'

interface Props {
  open: boolean
  onClose: () => void
}

export const CreateEducationDialog: React.FC<Props> = ({ open, onClose }) => {
  const [insertedData, insertMutation] = useInsertEducationOneMutation()

  const formik = useFormik({
    initialValues: {
      school: '',
      degree: '',
      fieldOfStudy: '',
      from: new Date(),
      to: '',
      description: ''
    },
    onSubmit: (values) => {
      toast.promise(useInsertEduationOne(values, insertMutation), {
        pending: 'Creating new education',
        success: 'New education created',
        error: 'Error while creating new education'
      })

      onClose()
      formik.resetForm()
    }
  })

  const handleClose = () => {
    onClose()
    formik.resetForm()
  }

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>Education</DialogTitle>
      <Box
        style={{
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingBottom: '50px'
        }}
      >
        <form onSubmit={formik.handleSubmit}>
          <Box
            sx={{
              width: '100%',
              height: '100%'
            }}
          >
            <TextField
              type="text"
              name="school"
              id="school"
              title="School"
              fullWidth
              required
              margin="normal"
              label="School"
              value={formik.values.school}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.school ? true : false
              }}
            />
            <TextField
              type="text"
              name="degree"
              id="degree"
              title="Degree"
              fullWidth
              required
              margin="normal"
              label="Degree"
              value={formik.values.degree}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.degree ? true : false
              }}
            />
            <TextField
              type="text"
              name="fieldOfStudy"
              id="fieldOfStudy"
              title="Field of Study"
              fullWidth
              required
              margin="normal"
              label="Field of Study"
              value={formik.values.fieldOfStudy}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.fieldOfStudy ? true : false
              }}
            />
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <Box
                style={{
                  paddingTop: '30px',
                  paddingBottom: '30px'
                }}
              >
                <DesktopDatePicker
                  label="From date"
                  inputFormat="dd.MM.yyyy"
                  maxDate={new Date()}
                  value={formik.values.from}
                  onChange={(value) => formik.setFieldValue('from', value)}
                  renderInput={(params) => <TextField required {...params} />}
                />
              </Box>
              <Box
                style={{
                  paddingBottom: '30px'
                }}
              >
                <DesktopDatePicker
                  label="To date"
                  inputFormat="dd.MM.yyyy"
                  value={formik.values.to}
                  onChange={(value) => formik.setFieldValue('to', value)}
                  renderInput={(params) => <TextField {...params} />}
                />
              </Box>
            </LocalizationProvider>
            <TextField
              id="description"
              label="Description"
              multiline
              fullWidth
              minRows={3}
              maxRows={6}
              value={formik.values.description}
              onChange={formik.handleChange}
              style={{
                paddingBottom: '30px'
              }}
            />
            <Box>
              <Button type="submit" variant="contained" fullWidth>
                Create Education
              </Button>
            </Box>
          </Box>
        </form>
      </Box>
    </Dialog>
  )
}
