import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogTitle,
  TextField
} from '@mui/material'
import { LocalizationProvider, DesktopDatePicker } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { useFormik } from 'formik'
import React, { useEffect } from 'react'
import { toast } from 'react-toastify'
import { useUpdateEducationOneMutation } from '../../generated/graphql'
import { useEducationOne, useUpdateEducationOne } from '../hooks'
import { EducationCard } from './EducationCard'

interface Props {
  id: any
  open: boolean
  onClose: () => void
  admin: boolean
  editing: boolean
}

export const EducationDialog: React.FC<Props> = ({
  id,
  open,
  onClose,
  admin,
  editing
}) => {
  const [updatedData, updateMutation] = useUpdateEducationOneMutation()
  const [data] = useEducationOne(id)

  useEffect(() => {
    formik.setFieldValue('school', data?.data?.educationOne?.school ?? '')
    formik.setFieldValue('degree', data?.data?.educationOne?.degree ?? '')
    formik.setFieldValue(
      'fieldOfStudy',
      data?.data?.educationOne?.fieldOfStudy ?? ''
    )
    formik.setFieldValue('from', data?.data?.educationOne?.from ?? '')
    formik.setFieldValue('to', data?.data?.educationOne?.to ?? '')
    formik.setFieldValue(
      'description',
      data?.data?.educationOne?.description ?? ''
    )
  }, [data?.data])

  const formik = useFormik({
    initialValues: {
      school: '',
      degree: '',
      fieldOfStudy: '',
      from: '',
      to: '',
      description: ''
    },
    onSubmit: (values) => {
      toast.promise(useUpdateEducationOne(values, id, updateMutation), {
        pending: 'Updating education',
        success: 'Education updated',
        error: 'Error while updating education'
      })

      handleClose()
    }
  })

  const handleClose = () => {
    onClose()
    formik.resetForm()
  }

  const handleAdmin = () => {
    if (admin && editing) {
      return (
        <Box
          style={{
            paddingLeft: '50px',
            paddingRight: '50px',
            paddingBottom: '50px'
          }}
        >
          <form onSubmit={formik.handleSubmit}>
            <Box
              sx={{
                width: '100%',
                height: '100%'
              }}
            >
              <TextField
                type="text"
                name="school"
                id="school"
                title="School"
                fullWidth
                required
                margin="normal"
                label="School"
                value={formik.values.school}
                onChange={formik.handleChange}
                InputLabelProps={{
                  shrink: formik.values.school ? true : false
                }}
              />
              <TextField
                type="text"
                name="degree"
                id="degree"
                title="Degree"
                fullWidth
                required
                margin="normal"
                label="Degree"
                value={formik.values.degree}
                onChange={formik.handleChange}
                InputLabelProps={{
                  shrink: formik.values.degree ? true : false
                }}
              />
              <TextField
                type="text"
                name="fieldOfStudy"
                id="fieldOfStudy"
                title="Field of Study"
                fullWidth
                required
                margin="normal"
                label="Field of Study"
                value={formik.values.fieldOfStudy}
                onChange={formik.handleChange}
                InputLabelProps={{
                  shrink: formik.values.fieldOfStudy ? true : false
                }}
              />
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <Box
                  style={{
                    paddingTop: '30px',
                    paddingBottom: '30px'
                  }}
                >
                  <DesktopDatePicker
                    label="From date"
                    inputFormat="dd.MM.yyyy"
                    maxDate={new Date()}
                    value={formik.values.from}
                    onChange={(value) => formik.setFieldValue('from', value)}
                    renderInput={(params) => <TextField required {...params} />}
                  />
                </Box>
                <Box
                  style={{
                    paddingBottom: '30px'
                  }}
                >
                  <DesktopDatePicker
                    label="To date"
                    inputFormat="dd.MM.yyyy"
                    value={formik.values.to}
                    onChange={(value) => formik.setFieldValue('to', value)}
                    renderInput={(params) => <TextField {...params} />}
                  />
                </Box>
              </LocalizationProvider>
              <TextField
                id="description"
                label="Description"
                multiline
                fullWidth
                minRows={3}
                maxRows={6}
                value={formik.values.description}
                onChange={formik.handleChange}
                style={{
                  paddingBottom: '30px'
                }}
              />
              <Box>
                <Button type="submit" variant="contained" fullWidth>
                  Update Education
                </Button>
              </Box>
            </Box>
          </form>
        </Box>
      )
    }

    return (
      <Box
        style={{
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingBottom: '50px'
        }}
      >
        <EducationCard
          data={data?.data?.educationOne}
          dialog={true}
          admin={false}
          // eslint-disable-next-line @typescript-eslint/no-empty-function
          onDelete={() => {}}
        />
      </Box>
    )
  }

  if (data?.fetching)
    return (
      <Dialog onClose={handleClose} open={open}>
        <DialogTitle>Education</DialogTitle>
        <Box
          style={{
            padding: '50px'
          }}
        >
          <CircularProgress />
        </Box>
      </Dialog>
    )

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>
        Education{' '}
        {data?.data?.educationOne?.degree
          ? ` - ${data?.data?.educationOne?.degree}`
          : ''}
      </DialogTitle>
      {handleAdmin()}
    </Dialog>
  )
}
