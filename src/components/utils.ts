import {
  APRIL,
  AUGUST,
  DECEMBER,
  FEBRUARY,
  JANUARY,
  JULY,
  JUNE,
  MARCH,
  MAY,
  NOVEMBER,
  OCTOBER,
  SEPTEMBER
} from './constants'

export const formatDate = (date: string | undefined) => {
  if (!date) return 'Present'
  const dateFormat = new Date(date)

  return dateFormat.getFullYear() + ' ' + handleMonth(dateFormat.getMonth())
}

export const monthDifference = (
  oldDate: string,
  newDate: string | undefined
) => {
  const dateFrom = new Date(oldDate)
  let dateTo
  if (!newDate) {
    dateTo = new Date()
  } else {
    dateTo = new Date(newDate)
  }

  const months =
    dateTo.getMonth() -
    dateFrom.getMonth() +
    (dateTo.getFullYear() - dateFrom.getFullYear()) * 12

  return months + 1
}

const handleMonth = (month: number) => {
  switch (month) {
    case 0:
      return JANUARY
    case 1:
      return FEBRUARY
    case 2:
      return MARCH
    case 3:
      return APRIL
    case 4:
      return MAY
    case 5:
      return JUNE
    case 6:
      return JULY
    case 7:
      return AUGUST
    case 8:
      return SEPTEMBER
    case 9:
      return OCTOBER
    case 10:
      return NOVEMBER
    case 11:
      return DECEMBER
  }
}
