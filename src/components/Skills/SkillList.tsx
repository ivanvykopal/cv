import { Box, CircularProgress, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { UseQueryState } from 'urql'
import {
  SkillGroupsQuery,
  useUpdateSkillMutation
} from '../../generated/graphql'
import { AddButton } from '../Admin'
import { Filter } from './Filter'
import './index.css'
import { SkillCard } from './SkillCard'
import { CreateSkillDialog } from './CreateSkillDialog'
import { toast } from 'react-toastify'
import { SkillsDialog } from './SkillsDialog'
import { useDeleteSkill } from '../hooks'

interface Props {
  data: UseQueryState<SkillGroupsQuery, object>
  admin: boolean
}

export const SkillList: React.FC<Props> = ({ data, admin }) => {
  const [id, setId] = useState(undefined)
  const [skillGroups, setSkillGroups] = useState(data?.data?.skillGroups)
  const [showDialog, setShowDialog] = useState<boolean>(false)
  const [showCreatingDialog, setShowCreatingDialog] = useState<boolean>(false)
  const [editing, setEditing] = useState(false)

  const [updateData, updateMutation] = useUpdateSkillMutation()

  useEffect(() => {
    setSkillGroups(data?.data?.skillGroups)
  }, [data?.data?.skillGroups])

  const createSkillItem = () => {
    const levels = new Map()
    skillGroups?.forEach((skillGroup) => {
      skillGroup.skills.forEach((skill) => {
        if (!skill.isDeleted) {
          if (!levels.has(skill.level)) {
            levels.set(skill.level, [skill])
          } else {
            levels.set(skill.level, [...levels.get(skill.level), skill])
          }
        }
      })
    })

    const skillsList: JSX.Element[] = []
    levels.forEach((level) => {
      level.forEach(
        (skill: {
          id: any | null | undefined
          name: string | null | undefined
          level: string | null | undefined
        }) => {
          skillsList.push(
            <Box
              className={`skill-item${admin ? '-active' : ''}`}
              key={skill.id}
              onClick={() => {
                setId(skill.id)
                setShowDialog(true)
              }}
            >
              <SkillCard
                id={skill.id}
                name={skill.name}
                level={skill.level}
                admin={admin}
                onDelete={onDelete}
                setEditing={(value) => {
                  setEditing(value)
                  setId(skill.id)
                  if (value) setShowDialog(true)
                }}
              />
            </Box>
          )
        }
      )
    })

    return skillsList
  }

  const onDelete = (e: any, id: any) => {
    e.stopPropagation()
    toast.promise(useDeleteSkill(id, updateMutation), {
      pending: 'Deleting skill',
      success: 'Skill deleted',
      error: 'Error while deleting skill'
    })
  }

  const closeDialog = () => {
    setId(undefined)
    setShowCreatingDialog(false)
    setShowDialog(false)
    setEditing(false)
  }

  const onFilter = (group: string) => {
    if (group === 'All') {
      setSkillGroups(data?.data?.skillGroups)
    } else {
      const result = data?.data?.skillGroups.filter(
        (skillGroup) => skillGroup.name === group
      )
      setSkillGroups(result)
    }
  }

  const handleAdminAdd = () => {
    if (admin)
      return (
        <Box
          style={{
            padding: '10px'
          }}
        >
          <AddButton onClick={(event) => setShowCreatingDialog(true)} />
        </Box>
      )
  }

  if (data.fetching) {
    return <CircularProgress />
  }

  return (
    <Box>
      <Box
        style={{
          display: 'flex',
          alignItems: 'center'
        }}
      >
        <Typography
          variant="h5"
          style={{
            fontWeight: 600
          }}
        >
          Skills
        </Typography>
        <Filter data={data?.data?.skillGroups} onFilter={onFilter} />
      </Box>
      {createSkillItem()}
      {handleAdminAdd()}
      <SkillsDialog
        id={id}
        open={admin && editing && showDialog}
        onClose={closeDialog}
        groups={data?.data?.skillGroups}
      />
      <CreateSkillDialog
        open={showCreatingDialog}
        onClose={closeDialog}
        groups={data?.data?.skillGroups}
      />
    </Box>
  )
}
