import React from 'react'
import { useFormik } from 'formik'

import {
  Box,
  Button,
  Dialog,
  DialogTitle,
  InputLabel,
  MenuItem,
  Select,
  TextField
} from '@mui/material'
import { toast } from 'react-toastify'
import { useInsertSkillOne } from '../hooks'
import { useInsertSkillMutation } from '../../generated/graphql'

interface Props {
  open: boolean
  onClose: () => void
  groups:
    | {
        id: string | number | readonly string[] | undefined
        name:
          | boolean
          | React.ReactChild
          | React.ReactFragment
          | React.ReactPortal
          | null
          | undefined
      }[]
    | undefined
}

const levels = ['Begginner', 'Basic', 'Skillful', 'Advanced', 'Expert']

export const CreateSkillDialog: React.FC<Props> = ({
  open,
  onClose,
  groups
}) => {
  const [insertedData, insertMutation] = useInsertSkillMutation()

  const formik = useFormik({
    initialValues: {
      skillGroupsId: '',
      level: '',
      name: ''
    },
    onSubmit: (values) => {
      toast.promise(useInsertSkillOne(values, insertMutation), {
        pending: 'Creating new skill',
        success: 'New skill created',
        error: 'Error while creating new skill'
      })

      handleClose()
    }
  })

  const handleClose = () => {
    onClose()
    formik.resetForm()
  }

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>Skill</DialogTitle>
      <Box
        style={{
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingBottom: '50px'
        }}
      >
        <form onSubmit={formik.handleSubmit}>
          <Box
            sx={{
              width: '100%',
              height: '100%'
            }}
          >
            <TextField
              type="text"
              name="name"
              id="name"
              title="Name"
              fullWidth
              required
              margin="normal"
              label="Name"
              value={formik.values.name}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.name ? true : false
              }}
            />
            <InputLabel shrink={true} id="select-skill-level-label">
              Skill Level
            </InputLabel>
            <Select
              fullWidth
              name="level"
              id="level"
              value={formik.values.level}
              onChange={formik.handleChange}
              labelId="select-skill-level-label"
              label="Skill level"
            >
              {levels.map((item, index) => {
                return (
                  <MenuItem value={item} key={index}>
                    {item}
                  </MenuItem>
                )
              })}
            </Select>
            <InputLabel shrink={true} id="select-skill-group-label">
              Skill Group
            </InputLabel>
            <Select
              fullWidth
              name="skillGroupsId"
              id="skillGroupsId"
              value={formik.values.skillGroupsId}
              onChange={formik.handleChange}
              labelId="select-skill-group-label"
              label="Skill group"
              required
            >
              {groups?.map((group, index: React.Key | null | undefined) => {
                return (
                  <MenuItem value={group.id} key={index}>
                    {group.name}
                  </MenuItem>
                )
              })}
            </Select>
            <Box
              style={{
                paddingTop: '30px'
              }}
            >
              <Button type="submit" variant="contained" fullWidth>
                Create Skill
              </Button>
            </Box>
          </Box>
        </form>
      </Box>
    </Dialog>
  )
}
