import { Box, FormControl, InputLabel, MenuItem, Select } from '@mui/material'
import { display } from '@mui/system'
import React, { useState } from 'react'

interface Props {
  data: any
  onFilter: (group: string) => void
}

export const Filter: React.FC<Props> = ({ data, onFilter }) => {
  const [group, setGroup] = useState('All')

  const handleChange = (event: any) => {
    setGroup(event.target.value)
    onFilter(event.target.value)
  }

  const menuList = () => {
    return data?.map((group: any) => {
      return (
        <MenuItem key={group.id} value={group.name}>
          {group.name}
        </MenuItem>
      )
    })
  }

  return (
    <Box
      style={{
        paddingLeft: '25%'
      }}
    >
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Choose Group</InputLabel>
        <Select
          label="Choose Group"
          style={{
            width: '250px',
            color: 'black'
          }}
          value={group}
          onChange={handleChange}
        >
          <MenuItem key={1} value={'All'}>
            All
          </MenuItem>
          {menuList()}
        </Select>
      </FormControl>
    </Box>
  )
}
