import { Box } from '@mui/material'
import React from 'react'
import { UseQueryState } from 'urql'
import { SkillGroupsQuery } from '../../generated/graphql'
import { SkillList } from './SkillList'

interface Props {
  data: UseQueryState<SkillGroupsQuery, object>
  admin: boolean
}
export const SkillsContainer: React.FC<Props> = ({ data, admin }) => {
  return (
    <Box
      id="skills"
      style={{
        marginTop: '25px'
      }}
    >
      <SkillList data={data} admin={admin} />
    </Box>
  )
}
