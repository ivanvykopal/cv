import { Box, Typography } from '@mui/material'
import React, { useState } from 'react'
import { DeleteButton, EditButton } from '../Admin'

interface Props {
  id: any
  name: string | null | undefined
  level: string | null | undefined
  admin: boolean
  onDelete?: (event: any, id: any) => void
  setEditing?: (value: boolean) => void
}

export const SkillCard: React.FC<Props> = ({
  id,
  name,
  level,
  admin,
  onDelete,
  setEditing
}) => {
  const [hover, setHover] = useState(false)

  const handleMouseIn = () => {
    setHover(true)
  }

  const handleMouseOut = () => {
    setHover(false)
  }

  const handleEdit = (event: any) => {
    event.stopPropagation()
    setEditing?.(true)
  }

  const handleAdmin = () => {
    if (admin && hover) {
      return (
        <Box
          style={{
            padding: '5px 5px'
          }}
        >
          <EditButton onClick={handleEdit} />
          <DeleteButton onClick={(event: any) => onDelete?.(event, id)} />
        </Box>
      )
    }
  }

  return (
    <Box
      onMouseOver={handleMouseIn}
      onMouseLeave={handleMouseOut}
      style={{
        display: 'flex',
        justifyContent: 'flex-end'
      }}
    >
      <Box style={{ marginRight: 'auto', padding: '10px 25px' }}>
        <Typography
          style={{
            display: 'inline',
            fontWeight: 600
          }}
        >
          {name}
        </Typography>
        <Typography
          style={{
            display: 'inline'
          }}
        >
          {level ? ` - ${level}` : ''}
        </Typography>
      </Box>
      {handleAdmin()}
    </Box>
  )
}
