import { Card, CardContent, CardHeader, Typography } from '@mui/material'
import React from 'react'
import { Fetching } from '../Dashboard/Fetching'

interface Props {
  fetching: boolean
  error: any
  children: JSX.Element
}

export const QueryBoundary = (props: Props) => {
  if (props.fetching) {
    return <Fetching />
  }

  if (props.error) {
    return (
      <Card
        style={{
          margin: '25px',
          maxWidth: '75%'
        }}
      >
        <CardHeader
          style={{ background: 'red', color: 'white' }}
          title={'An error occurred!'}
        />
        <CardContent>
          <Typography>{props.error.message}</Typography>
        </CardContent>
      </Card>
    )
  }

  return props.children
}
