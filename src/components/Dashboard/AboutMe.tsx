import { Box, Typography } from '@mui/material'
import React from 'react'
import about from './about'

export const AboutMe: React.FC = () => {
  return (
    <Box>
      <Typography
        variant="h6"
        style={{
          fontWeight: 600
        }}
      >
        {'About me:'}
      </Typography>
      <Typography
        style={{
          textAlign: 'justify',
          overflowWrap: 'break-word',
          marginRight: '10%',
          marginTop: '15px'
        }}
      >
        {about}
      </Typography>
    </Box>
  )
}
