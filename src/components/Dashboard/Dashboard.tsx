import { Box } from '@mui/material'
import React from 'react'
import { EducationContainer } from '../Education/EducationContainer'
import { QueryBoundary } from '../Errors/QueryBoundary'
import { ExperienceContainer } from '../Experience/ExperienceContainer'
import {
  useEducationList,
  useExperienceList,
  useProjectsList,
  useSkillGroupsList
} from '../hooks'
import { InformationContainer } from '../Information/InformationContainer'
import { ProjectsContainer } from '../Projects/ProjectsContainer'
import { SkillsContainer } from '../Skills/SkillsContainer'
import { AboutMe } from './AboutMe'
import { CustomDivider } from './CustomDivider'

interface Props {
  admin: boolean
}

const Dashboard: React.FC<Props> = ({ admin }) => {
  const [educationList] = useEducationList()

  const [experienceList] = useExperienceList()

  const [skillGroups] = useSkillGroupsList()

  const [projects] = useProjectsList()

  const handleError = () => {
    return (
      experienceList.error ||
      educationList.error ||
      skillGroups.error ||
      projects.error
    )
  }

  const handleFetching = () => {
    return (
      experienceList.fetching ||
      educationList.fetching ||
      skillGroups.fetching ||
      projects.fetching
    )
  }

  return (
    <Box
      style={{
        paddingLeft: '2rem',
        paddingTop: '5rem'
      }}
    >
      <Box
        style={{
          display: 'block',
          position: 'sticky',
          top: '5rem'
        }}
      >
        <InformationContainer />
      </Box>
      <Box
        style={{
          display: 'block',
          paddingLeft: '30%'
        }}
      >
        <AboutMe />
        <QueryBoundary error={handleError()} fetching={handleFetching()}>
          <Box>
            <ExperienceContainer data={experienceList} admin={admin} />
            <CustomDivider />
            <EducationContainer data={educationList} admin={admin} />
            <CustomDivider />
            <SkillsContainer data={skillGroups} admin={admin} />
            <CustomDivider />
            <ProjectsContainer data={projects} admin={admin} />
          </Box>
        </QueryBoundary>
      </Box>
    </Box>
  )
}

export default Dashboard
