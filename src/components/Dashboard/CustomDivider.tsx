import React from 'react'
import Divider from '@mui/material/Divider'

export const CustomDivider: React.FC = () => {
  return (
    <Divider
      style={{
        paddingTop: '25px'
      }}
    />
  )
}
