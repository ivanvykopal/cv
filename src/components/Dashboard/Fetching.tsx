import { Box, CircularProgress } from '@mui/material'
import React from 'react'

export const Fetching: React.FC = () => {
  return (
    <Box
      style={{
        paddingTop: '50px'
      }}
    >
      <CircularProgress />
    </Box>
  )
}
