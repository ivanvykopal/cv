import { Box } from '@mui/material'
import React from 'react'
import { UseQueryState } from 'urql'
import { ExperienceListQuery } from '../../generated/graphql'
import { ExperienceList } from './ExperienceList'

interface Props {
  data: UseQueryState<ExperienceListQuery, object>
  admin: boolean
}

export const ExperienceContainer: React.FC<Props> = ({ data, admin }) => {
  return (
    <Box
      id="experience"
      style={{
        marginTop: '25px'
      }}
    >
      <ExperienceList data={data} admin={admin} />
    </Box>
  )
}
