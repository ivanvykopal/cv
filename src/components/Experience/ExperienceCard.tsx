import { Box, CircularProgress, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { DeleteButton, EditButton } from '../Admin'
import { formatDate, monthDifference } from '../utils'

interface Props {
  data: any
  dialog: boolean
  admin: boolean
  onDelete?: (event: any, id: any) => void
  setEditing?: (value: boolean) => void
}

export const ExperienceCard: React.FC<Props> = ({
  data,
  dialog,
  admin,
  onDelete,
  setEditing
}) => {
  const [experience, setExperience] = useState(data)
  const [hover, setHover] = useState(false)

  useEffect(() => {
    setExperience(data)
  }, [data])

  const handleMouseIn = () => {
    setHover(true)
  }

  const handleMouseOut = () => {
    setHover(false)
  }

  const handleEdit = (event: any) => {
    event.stopPropagation()
    setEditing?.(true)
  }

  const handleAdmin = () => {
    if (admin && hover) {
      return (
        <Box
          style={{
            padding: '5px 5px'
          }}
        >
          <EditButton onClick={handleEdit} />
          <DeleteButton
            onClick={(event: any) => onDelete?.(event, experience.id)}
          />
        </Box>
      )
    }
  }

  const showNoteIfExist = () => {
    if (dialog && experience?.note) {
      return (
        <Box
          style={{
            paddingTop: '25px'
          }}
        >
          <Typography
            style={{
              fontSize: 18,
              fontWeight: 600
            }}
          >
            Note:{' '}
          </Typography>
          <Typography>{experience.note}</Typography>
        </Box>
      )
    }
  }

  if (dialog && !experience?.name) {
    return <CircularProgress />
  }

  return (
    <Box
      onMouseOver={handleMouseIn}
      onMouseLeave={handleMouseOut}
      style={{
        display: 'flex',
        justifyContent: 'flex-end'
      }}
    >
      <Box style={{ marginRight: 'auto', padding: '10px 25px' }}>
        <Typography
          style={{
            fontWeight: 800
          }}
        >
          {experience?.name}
        </Typography>
        <Typography
          style={{
            fontWeight: 600,
            display: 'inline'
          }}
        >
          {`${experience?.firmName} ~ `}
        </Typography>
        <Typography
          style={{
            display: 'inline'
          }}
        >
          {`${experience?.type}`}
        </Typography>
        <Typography>
          {`${formatDate(experience?.from)} - ${formatDate(
            experience?.to
          )} ~ ${monthDifference(experience?.from, experience?.to)} mes`}
        </Typography>
        {showNoteIfExist()}
      </Box>
      {handleAdmin()}
    </Box>
  )
}
