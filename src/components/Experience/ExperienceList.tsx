import { Box, CircularProgress, Typography } from '@mui/material'
import React, { useState } from 'react'
import { toast } from 'react-toastify'
import { UseQueryState } from 'urql'
import {
  ExperienceListQuery,
  useUpdateExperienceOneMutation
} from '../../generated/graphql'
import { AddButton } from '../Admin'
import { useDeleteExperience } from '../hooks'
import { CreateExperienceDialog } from './CreateExperienceDialog'
import { ExperienceCard } from './ExperienceCard'
import { ExperienceDialog } from './ExperienceDialog'
import './index.css'

interface Props {
  data: UseQueryState<ExperienceListQuery, object>
  admin: boolean
}

export const ExperienceList: React.FC<Props> = ({ data, admin }) => {
  const [id, setId] = useState(undefined)
  const [showDialog, setShowDialog] = useState<boolean>(false)
  const [showCreatingDialog, setShowCreatingDialog] = useState<boolean>(false)
  const [editing, setEditing] = useState(false)

  const [updateData, updateMutation] = useUpdateExperienceOneMutation()

  const closeDialog = () => {
    setShowDialog(false)
    setShowCreatingDialog(false)
    setId(undefined)
    setEditing(false)
  }

  const onDelete = (e: any, id: any) => {
    e.stopPropagation()
    toast.promise(useDeleteExperience(id, updateMutation), {
      pending: 'Deleting experience',
      success: 'Experience deleted',
      error: 'Error while deleting experience'
    })
  }

  const createExperienceItems = () => {
    return data.data?.experienceList.map((experience) => {
      return (
        <Box
          className="experience-item"
          key={experience.id}
          onClick={() => {
            setId(experience.id)
            setShowDialog(true)
          }}
        >
          <ExperienceCard
            data={experience}
            dialog={false}
            admin={admin}
            onDelete={onDelete}
            setEditing={(value) => {
              setEditing(value)
              setId(experience.id)
              if (value) setShowDialog(true)
            }}
          />
        </Box>
      )
    })
  }

  const handleAdminAdd = () => {
    if (admin)
      return (
        <Box
          style={{
            padding: '10px'
          }}
        >
          <AddButton onClick={() => setShowCreatingDialog(true)} />
        </Box>
      )
  }

  if (data.fetching) {
    return <CircularProgress />
  }

  return (
    <Box>
      <Box
        style={{
          alignItems: 'center'
        }}
      >
        <Typography
          variant="h5"
          style={{
            fontWeight: 600
          }}
        >
          Work Experience
        </Typography>
      </Box>
      {createExperienceItems()}
      <ExperienceDialog
        id={id}
        open={showDialog}
        onClose={closeDialog}
        admin={admin}
        editing={editing}
      />
      <CreateExperienceDialog
        open={admin && showCreatingDialog}
        onClose={closeDialog}
      />
      {handleAdminAdd()}
    </Box>
  )
}
