import { Box, Button, Dialog, DialogTitle, TextField } from '@mui/material'
import { useFormik } from 'formik'
import React from 'react'
import { toast } from 'react-toastify'
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker'
import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { useInsertExperienceOne } from '../hooks'
import { useInsertExperienceOneMutation } from '../../generated/graphql'

interface Props {
  open: boolean
  onClose: () => void
}

export const CreateExperienceDialog: React.FC<Props> = ({ open, onClose }) => {
  const [insertedData, insertMutation] = useInsertExperienceOneMutation()

  const formik = useFormik({
    initialValues: {
      name: '',
      firmName: '',
      type: '',
      position: '',
      from: new Date(),
      to: '',
      note: ''
    },
    onSubmit: (values) => {
      toast.promise(useInsertExperienceOne(values, insertMutation), {
        pending: 'Creating new experience',
        success: 'New experience created',
        error: 'Error while creating new experience'
      })

      onClose()
      formik.resetForm()
    }
  })

  const handleClose = () => {
    onClose()
    formik.resetForm()
  }

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>Experience</DialogTitle>
      <Box
        style={{
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingBottom: '50px'
        }}
      >
        <form onSubmit={formik.handleSubmit}>
          <Box
            sx={{
              width: '100%',
              height: '100%'
            }}
          >
            <TextField
              type="text"
              name="name"
              id="name"
              title="Name"
              fullWidth
              required
              margin="normal"
              label="Name"
              value={formik.values.name}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.name ? true : false
              }}
            />
            <TextField
              type="text"
              name="firmName"
              id="firmName"
              title="Firm Name"
              fullWidth
              required
              margin="normal"
              label="Firm Name"
              value={formik.values.firmName}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.firmName ? true : false
              }}
            />
            <TextField
              type="text"
              name="type"
              id="type"
              title="Type"
              fullWidth
              required
              margin="normal"
              label="Type"
              value={formik.values.type}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.type ? true : false
              }}
            />
            <TextField
              type="text"
              name="position"
              id="position"
              title="Position"
              fullWidth
              required
              margin="normal"
              label="Position"
              value={formik.values.position}
              onChange={formik.handleChange}
              InputLabelProps={{
                shrink: formik.values.position ? true : false
              }}
            />
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <Box
                style={{
                  paddingTop: '30px',
                  paddingBottom: '30px'
                }}
              >
                <DesktopDatePicker
                  label="From date"
                  inputFormat="dd.MM.yyyy"
                  maxDate={new Date()}
                  value={formik.values.from}
                  onChange={(value) => formik.setFieldValue('from', value)}
                  renderInput={(params) => <TextField required {...params} />}
                />
              </Box>
              <Box
                style={{
                  paddingBottom: '30px'
                }}
              >
                <DesktopDatePicker
                  label="To date"
                  inputFormat="dd.MM.yyyy"
                  value={formik.values.to}
                  onChange={(value) => formik.setFieldValue('to', value)}
                  renderInput={(params) => <TextField {...params} />}
                />
              </Box>
            </LocalizationProvider>
            <TextField
              id="note"
              label="Note"
              multiline
              fullWidth
              minRows={3}
              maxRows={6}
              value={formik.values.note}
              onChange={formik.handleChange}
              style={{
                paddingBottom: '30px'
              }}
            />
            <Box>
              <Button type="submit" variant="contained" fullWidth>
                Create Experience
              </Button>
            </Box>
          </Box>
        </form>
      </Box>
    </Dialog>
  )
}
