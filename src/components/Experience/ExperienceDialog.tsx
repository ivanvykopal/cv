import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogTitle,
  TextField
} from '@mui/material'
import { LocalizationProvider, DesktopDatePicker } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { useFormik } from 'formik'
import React, { useEffect } from 'react'
import { toast } from 'react-toastify'
import { useUpdateExperienceOneMutation } from '../../generated/graphql'
import { useExperienceOne, useUpdateExperienceOne } from '../hooks'
import { ExperienceCard } from './ExperienceCard'

interface Props {
  id: any
  open: boolean
  admin: boolean
  onClose: () => void
  editing: boolean
}

export const ExperienceDialog: React.FC<Props> = ({
  id,
  open,
  onClose,
  admin,
  editing
}) => {
  const [updatedData, updateMutation] = useUpdateExperienceOneMutation()
  const [data] = useExperienceOne(id)

  useEffect(() => {
    formik.setFieldValue('name', data?.data?.experienceOne?.name ?? '')
    formik.setFieldValue('firmName', data?.data?.experienceOne?.firmName ?? '')
    formik.setFieldValue('type', data?.data?.experienceOne?.type ?? '')
    formik.setFieldValue('position', data?.data?.experienceOne?.position ?? '')
    formik.setFieldValue('from', data?.data?.experienceOne?.from ?? '')
    formik.setFieldValue('to', data?.data?.experienceOne?.to ?? '')
    formik.setFieldValue('note', data?.data?.experienceOne?.note ?? '')
  }, [data?.data])

  const formik = useFormik({
    initialValues: {
      name: '',
      firmName: '',
      type: '',
      position: '',
      from: '',
      to: '',
      note: ''
    },
    onSubmit: (values) => {
      toast.promise(useUpdateExperienceOne(values, id, updateMutation), {
        pending: 'Updating experience',
        success: 'Experience updated',
        error: 'Error while updating experience'
      })

      handleClose()
    }
  })

  const handleClose = () => {
    onClose()
    formik.resetForm()
  }

  const handleAdmin = () => {
    if (admin && editing) {
      return (
        <Box
          style={{
            paddingLeft: '50px',
            paddingRight: '50px',
            paddingBottom: '50px'
          }}
        >
          <form onSubmit={formik.handleSubmit}>
            <Box
              sx={{
                width: '100%',
                height: '100%'
              }}
            >
              <TextField
                type="text"
                name="name"
                id="name"
                title="Name"
                fullWidth
                required
                margin="normal"
                label="Name"
                value={formik.values.name}
                onChange={formik.handleChange}
                InputLabelProps={{
                  shrink: formik.values.name ? true : false
                }}
              />
              <TextField
                type="text"
                name="firmName"
                id="firmName"
                title="Firm Name"
                fullWidth
                required
                margin="normal"
                label="Firm Name"
                value={formik.values.firmName}
                onChange={formik.handleChange}
                InputLabelProps={{
                  shrink: formik.values.firmName ? true : false
                }}
              />
              <TextField
                type="text"
                name="type"
                id="type"
                title="Type"
                fullWidth
                required
                margin="normal"
                label="Type"
                value={formik.values.type}
                onChange={formik.handleChange}
                InputLabelProps={{
                  shrink: formik.values.type ? true : false
                }}
              />
              <TextField
                type="text"
                name="position"
                id="position"
                title="Position"
                fullWidth
                required
                margin="normal"
                label="Position"
                value={formik.values.position}
                onChange={formik.handleChange}
                InputLabelProps={{
                  shrink: formik.values.position ? true : false
                }}
              />
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <Box
                  style={{
                    paddingTop: '30px',
                    paddingBottom: '30px'
                  }}
                >
                  <DesktopDatePicker
                    label="From date"
                    inputFormat="dd.MM.yyyy"
                    maxDate={new Date()}
                    value={formik.values.from}
                    onChange={(value) => formik.setFieldValue('from', value)}
                    renderInput={(params) => <TextField required {...params} />}
                  />
                </Box>
                <Box
                  style={{
                    paddingBottom: '30px'
                  }}
                >
                  <DesktopDatePicker
                    label="To date"
                    inputFormat="dd.MM.yyyy"
                    value={formik.values.to}
                    onChange={(value) => formik.setFieldValue('to', value)}
                    renderInput={(params) => <TextField {...params} />}
                  />
                </Box>
              </LocalizationProvider>
              <TextField
                id="note"
                label="Note"
                multiline
                fullWidth
                minRows={3}
                maxRows={6}
                value={formik.values.note}
                onChange={formik.handleChange}
                style={{
                  paddingBottom: '30px'
                }}
              />
              <Box>
                <Button type="submit" variant="contained" fullWidth>
                  Update Experience
                </Button>
              </Box>
            </Box>
          </form>
        </Box>
      )
    }

    return (
      <Box
        style={{
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingBottom: '50px'
        }}
      >
        <ExperienceCard
          data={data?.data?.experienceOne}
          dialog={true}
          admin={false}
        />
      </Box>
    )
  }

  if (data?.fetching)
    return (
      <Dialog onClose={handleClose} open={open}>
        <DialogTitle>Experience</DialogTitle>
        <Box
          style={{
            padding: '50px'
          }}
        >
          <CircularProgress />
        </Box>
      </Dialog>
    )

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>
        Experience{' '}
        {data?.data?.experienceOne?.name
          ? ` - ${data?.data?.experienceOne?.name}`
          : ''}
      </DialogTitle>
      {handleAdmin()}
    </Dialog>
  )
}
