import { Navigate } from 'react-router-dom'
import { PageNotFound } from './components/404/PageNotFound'
import Dashboard from './components/Dashboard/Dashboard'
import MainLayout from './components/MainLayout/MainLayout'

const routes = [
  {
    element: <MainLayout />,
    children: [
      {
        path: '/cv',
        element: <Dashboard admin={false} />
      },
      {
        path: '/admin',
        element: <Dashboard admin={true} />
      },
      {
        path: '/404',
        element: <PageNotFound />
      },
      {
        path: '/*',
        element: <Navigate to="/404" />
      },
      {
        path: '/',
        element: <Navigate to="/cv" />
      }
    ]
  }
]

export default routes
