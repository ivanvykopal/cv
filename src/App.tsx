import { useRoutes } from 'react-router-dom'
import { TopBar } from './components/TopBar/TopBar'
import routes from './routes'
import {
  createClient,
  Provider as UrqlProvider,
  debugExchange,
  cacheExchange,
  fetchExchange
} from 'urql'

const client = createClient({
  url: import.meta.env.VITE_HASURA_GRAPHQL_ENDPOINT as string,
  exchanges: [debugExchange, cacheExchange, fetchExchange]
})

const App = () => {
  const routing = useRoutes(routes)

  return <UrqlProvider value={client}>{routing}</UrqlProvider>
}

export default App
